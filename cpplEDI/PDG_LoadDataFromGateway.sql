USE [cpplEDI]
GO

/****** Object:  StoredProcedure [dbo].[PDG_LoadDatafromGateway]    Script Date: 23/11/2020 3:31:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[PDG_LoadDatafromGateway] 
AS
BEGIN

execute dbo.PDG_Drop_FK_Constraints;



SELECT 'ccreweigh';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ccreweigh') DROP TABLE [ccreweigh];
CREATE TABLE [ccreweigh] (
 [ccr_coupon] char(32) NOT NULL,
  [ccr_coupon_id] int NOT NULL,
  [ccr_stamp] datetime default NULL,
  [ccr_location] char(16) DEFAULT '',
  [ccr_deadweight] FLOAT(25) DEFAULT '0',
  [ccr_dimension0] FLOAT(25) DEFAULT '0',
  [ccr_dimension1] FLOAT(25) DEFAULT '0',
  [ccr_dimension2] FLOAT(25) DEFAULT '0',
  [ccr_volume] FLOAT(25) DEFAULT '0',
  PRIMARY KEY ([ccr_coupon_id])
) ;
BULK INSERT [ccreweigh] FROM 'E:\ETL Processing\Gateway\Import\Extract\ccreweigh.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors110.txt');

--------------------------------


SELECT 'ccreweighhold';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ccreweighhold') DROP TABLE [ccreweighhold];
CREATE TABLE [ccreweighhold] (
 [crh_coupon] char(32) NOT NULL DEFAULT '',
  [crh_coupon_id] int NOT NULL,
  [crh_stamp] datetime default NULL,
  [crh_checked] char(1) DEFAULT 'N',
  [crh_location] char(16) DEFAULT '',
  [crh_deadweight] FLOAT(25) DEFAULT '0',
  [crh_dimension0] FLOAT(25) DEFAULT '0',
  [crh_dimension1] FLOAT(25) DEFAULT '0',
  [crh_dimension2] FLOAT(25) DEFAULT '0',
  [crh_volume] FLOAT(25) DEFAULT '0',
  PRIMARY KEY ([crh_coupon]),
) ;
BULK INSERT [ccreweighhold] FROM 'E:\ETL Processing\Gateway\Import\Extract\ccreweighhold.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors111.txt');

------------------------


SELECT 'cdreweigh';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdreweigh') DROP TABLE [cdreweigh];
CREATE TABLE [cdreweigh] (
 [cr_consignment] int NOT NULL,
  [cr_stamp] datetime default NULL,
  [cr_deadweight] FLOAT(25) DEFAULT '0',
  [cr_volume] FLOAT(25) DEFAULT '0',
  [cr_chargeweight] FLOAT(25) DEFAULT '0',
  PRIMARY KEY ([cr_consignment])
) ;
BULK INSERT [cdreweigh] FROM 'E:\ETL Processing\Gateway\Import\Extract\cdreweigh.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors112.txt');

--------------------------


SELECT 'addresses';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='addresses') DROP TABLE [addresses];
CREATE TABLE [addresses] (
  [a_id] int NOT NULL,
  [a_company_id] int DEFAULT '0',
  [a_user] varchar(32) DEFAULT '',
  [a_pickup] char(1) DEFAULT 'N',
  [a_delivery] char(1) DEFAULT 'N',
  [a_code] varchar(40) DEFAULT NULL,
  [a_addr0] varchar(40) DEFAULT '',
  [a_addr1] varchar(40) DEFAULT '',
  [a_addr2] varchar(40) DEFAULT '',
  [a_addr3] varchar(40) DEFAULT '',
  [a_suburb] varchar(30) DEFAULT '',
  [a_postcode] int DEFAULT '0',
  [a_contact] varchar(40) DEFAULT '',
  [a_phone] varchar(20) DEFAULT '',
  [a_email] varchar(255) DEFAULT '',
  PRIMARY KEY ([a_id]),
) ;
BULK INSERT [addresses] FROM 'E:\ETL Processing\Gateway\Import\Extract\addresses.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors1.txt');

--------------------------


SELECT 'agedbalances';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agedbalances') DROP TABLE [agedbalances];
CREATE TABLE [agedbalances] (
  [a_company_id] int NOT NULL DEFAULT '0',
  [a_current] FLOAT(25) DEFAULT '0',
  [a_7days] FLOAT(25) DEFAULT '0',
  [a_14days] FLOAT(25) DEFAULT '0',
  [a_21days] FLOAT(25) DEFAULT '0',
  [a_28days] FLOAT(25) DEFAULT '0',
  [a_60days] FLOAT(25) DEFAULT '0',
  [a_90days] FLOAT(25) DEFAULT '0',
  [a_over] FLOAT(25) DEFAULT '0',
  [a_total] FLOAT(25) DEFAULT '0',
  [a_asat] SMALLDATETIME default NULL,
  PRIMARY KEY ([a_company_id])
) ;
BULK INSERT [agedbalances] FROM 'E:\ETL Processing\Gateway\Import\Extract\agedbalances.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors2.txt');


--------------------------

SELECT 'agentemail';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentemail') DROP TABLE [agentemail];
CREATE TABLE [agentemail] (
    [ae_agent] int NOT NULL DEFAULT '0',
  [ae_description] varchar(64) NOT NULL DEFAULT '',
  [ae_email] varchar(128) NOT NULL DEFAULT '',
  [ae_class] int NOT NULL DEFAULT '0',
  [ae_style] char(1) DEFAULT 'N',
) ;
BULK INSERT [agentemail] FROM 'E:\ETL Processing\Gateway\Import\Extract\agentemail.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors3.txt');


--------------------------


SELECT 'agentinfo';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentinfo') DROP TABLE [agentinfo];
CREATE TABLE [agentinfo] (
  [zoning_id] int NOT NULL DEFAULT '0',
  [postcode] int DEFAULT NULL,
  [suburb] varchar(64) DEFAULT NULL,
  [state] varchar(10) DEFAULT NULL,
  [agent_id] int NOT NULL DEFAULT '0',
  [alt_agent_id] int NOT NULL DEFAULT '0',
  [zoneid] int DEFAULT '0',
  [subzoneid] int DEFAULT '0',
  [branch] int DEFAULT NULL,
  [depot] int DEFAULT NULL,
  [cppldriver] int DEFAULT '0',
  [ai_id] int NOT NULL,
  [ai_eta_zone] varchar(12) DEFAULT '',
  [ai_sort_code] varchar(12) DEFAULT NULL,
  [a_imported] char(1) DEFAULT 'N',
  [ai_sort_nsw] char(1) DEFAULT '',
  [ai_sort_act] char(1) DEFAULT '',
  [ai_sort_vic] char(1) DEFAULT '',
  [ai_sort_tas] char(1) DEFAULT '',
  [ai_sort_qld] char(1) DEFAULT '',
  [ai_sort_sa] char(1) DEFAULT '',
  [ai_sort_nt] char(1) DEFAULT '',
  [ai_sort_wa] char(1) DEFAULT '',
  [ai_sort_suffix] char(8) DEFAULT '',
  [ai_eta_range_code] char(12) DEFAULT '',
  PRIMARY KEY ([ai_id]),
) ;
BULK INSERT [agentinfo] FROM 'E:\ETL Processing\Gateway\Import\Extract\agentinfo.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors4.txt');

--------------------------



SELECT 'agentinfo2';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentinfo2') DROP TABLE [agentinfo2];
CREATE TABLE [agentinfo2] (
   [ai_id] int NOT NULL,
  [agent_id] int NOT NULL DEFAULT '0',
  [pricing_id] char(4) NOT NULL DEFAULT '',
  [postcode] int DEFAULT NULL,
  [suburb] char(32) DEFAULT NULL,
  [state] char(10) DEFAULT NULL,
  [metro] int DEFAULT NULL,
  [links] int DEFAULT NULL,
  [zone] char(10) DEFAULT NULL,
  [zoneid] int DEFAULT '0',
  [depot] int DEFAULT NULL,
  [branch] int DEFAULT NULL,
  PRIMARY KEY ([ai_id]),
) ;
BULK INSERT [agentinfo2] FROM 'E:\ETL Processing\Gateway\Import\Extract\agentinfo2.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors5.txt');

-------------------------


SELECT 'agentinfo3';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agentinfo3') DROP TABLE [agentinfo3];
CREATE TABLE [agentinfo3] (
 [zoning_id] int NOT NULL DEFAULT '0',
  [postcode] int DEFAULT NULL,
  [suburb] varchar(64) DEFAULT NULL,
  [state] varchar(10) DEFAULT NULL,
  [agent_id] int NOT NULL DEFAULT '0',
  [zoneid] int DEFAULT '0',
  [subzoneid] int DEFAULT '0',
  [branch] int DEFAULT NULL,
  [depot] int DEFAULT NULL,
  [cppldriver] int DEFAULT '0',
  [ai_id] int NOT NULL DEFAULT '0',
  [ai_eta_zone] varchar(12) DEFAULT '',
  [ai_sort_code] varchar(12) DEFAULT NULL,
  [a_imported] char(1) DEFAULT 'N',
  [ai_sort_nsw] char(1) DEFAULT '',
  [ai_sort_act] char(1) DEFAULT '',
  [ai_sort_vic] char(1) DEFAULT '',
  [ai_sort_tas] char(1) DEFAULT '',
  [ai_sort_qld] char(1) DEFAULT '',
  [ai_sort_sa] char(1) DEFAULT '',
  [ai_sort_nt] char(1) DEFAULT '',
  [ai_sort_wa] char(1) DEFAULT '',
  [ai_sort_suffix] char(8) DEFAULT ''
) ;
BULK INSERT [agentinfo3] FROM 'E:\ETL Processing\Gateway\Import\Extract\agentinfo3.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors6.txt');

-------------------------


SELECT 'agents';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='agents') DROP TABLE [agents];
CREATE TABLE [agents] (
  [a_id] int NOT NULL,
  [a_shortname] varchar(32) NOT NULL DEFAULT '',
  [a_name] varchar(64) NOT NULL DEFAULT '',
  [a_supplier_code] varchar(32) DEFAULT '',
  [a_podrequired] char(1) DEFAULT 'N',
  [a_cppl] char(1) DEFAULT 'N',
  [a_querybranch] int DEFAULT NULL,
  [a_driver] int DEFAULT '0',
  [a_label_email] varchar(255) DEFAULT NULL,
  [a_label_printer] varchar(255) DEFAULT NULL,
  [a_booking_branch] int DEFAULT NULL,
  [a_booking_email] varchar(255) DEFAULT NULL,
  [a_returns_direct] char(1) DEFAULT 'N',
  [a_ezyfreight_direct] char(1) DEFAULT 'N',
  [a_import_direct] char(1) DEFAULT 'N',
  [a_auth_owner] varchar(32) DEFAULT '',
  [a_auth_state] varchar(32) DEFAULT '',
  [a_auth_company] varchar(32) DEFAULT '',
  [a_ogm_trigger] char(1) DEFAULT 'D',
  [a_ogm_data] varchar(255) DEFAULT '',
  [a_ogm_runs] varchar(8) DEFAULT 'YNNNNNNN',
  [a_split_returns] char(1) DEFAULT 'N',
  [a_style] char(1) DEFAULT '',
  [a_ap] char(1) DEFAULT 'N',
  [a_apinfo] char(16) DEFAULT '',
  PRIMARY KEY ([a_id])
) ;
BULK INSERT [agents] FROM 'E:\ETL Processing\Gateway\Import\Extract\agents.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors7.txt');

-------------------------


SELECT 'apconnote';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='apconnote') DROP TABLE [apconnote];
CREATE TABLE [apconnote] (
   [ac_id] int NOT NULL,
  [ac_connote] char(16) NOT NULL,
  [ac_date] SMALLDATETIME default NULL,
  [ac_consignment] int NOT NULL,
  [ac_sender_name] char(40) DEFAULT '',
  [ac_sender_addr1] char(40) DEFAULT '',
  [ac_sender_addr2] char(40) DEFAULT '',
  [ac_sender_suburb] char(40) DEFAULT '',
  [ac_sender_postcode] char(10) DEFAULT '',
  [ac_receiver_name] char(40) DEFAULT '',
  [ac_receiver_addr1] char(40) DEFAULT '',
  [ac_receiver_addr2] char(40) DEFAULT '',
  [ac_receiver_suburb] char(40) DEFAULT '',
  [ac_receiver_postcode] char(10) DEFAULT '',
  [ac_atl] char(1) DEFAULT 'N',
  [ac_special_instructions] char(120) DEFAULT '',
  [ac_manifest_stamp] datetime default NULL,
  [ac_manifest_id] int DEFAULT '0',
  [ac_sender_location] char(16) DEFAULT '',
  [ac_receiver_location] char(16) DEFAULT '',
  PRIMARY KEY ([ac_id]),
) ;
BULK INSERT [apconnote] FROM 'E:\ETL Processing\Gateway\Import\Extract\apconnote.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors8.txt');

-------------------------



SELECT 'apinfo';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='apinfo') DROP TABLE [apinfo];
CREATE TABLE [apinfo] (
   [ai_sender_id] char(16) NOT NULL,
  [ai_sender_name] char(40) DEFAULT '',
  [ai_sender_addr1] char(40) DEFAULT '',
  [ai_sender_addr2] char(40) DEFAULT '',
  [ai_sender_suburb] char(40) DEFAULT '',
  [ai_sender_postcode] int DEFAULT '0',
  [ai_sender_record_no] int DEFAULT '0',
  [ai_connote_prefix] char(8) DEFAULT '',
  [ai_connote_id] int DEFAULT '1000000',
  [ai_connote_start] int DEFAULT '1000000',
  [ai_connote_finish] int DEFAULT '9999999',
  [ai_merchant_location_id] char(16) DEFAULT '',
  [ai_manifest_filename_prefix] char(16) DEFAULT '',
  [ai_manifest_id] int DEFAULT '0',
  [ai_merchant_id] char(16) DEFAULT '',
  [ai_charge_code] char(16) DEFAULT '',
  [ai_charge_description] char(40) DEFAULT '',
  [ai_transaction_id] int DEFAULT '0',
  [ai_post_account] char(20) DEFAULT '',
  [ai_lodgement_facility] char(80) DEFAULT '',
  [ai_manifest_username] char(32) DEFAULT '',
  [ai_transfer_host] varchar(256) DEFAULT NULL,
  [ai_transfer_username] varchar(256) DEFAULT NULL,
  [ai_transfer_password] varchar(256) DEFAULT NULL,
  PRIMARY KEY ([ai_sender_id])
) ;
BULK INSERT [apinfo] FROM 'E:\ETL Processing\Gateway\Import\Extract\apinfo.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors9.txt');


-------------------------


SELECT 'aplink';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='aplink') DROP TABLE [aplink];
CREATE TABLE [aplink] (
   [al_id] int NOT NULL,
  [al_cpl_connote] char(32) NOT NULL,
  [al_cpl_label] char(32) NOT NULL,
  [al_cpl_id] int DEFAULT '0',
  [al_ap_connote] char(16) NOT NULL,
  [al_ap_label] char(16) NOT NULL,
  [al_ap_no] int NOT NULL,
  [al_ap_id] int DEFAULT '0',
  [al_status] char(1) DEFAULT 'N',
  [al_weight] FLOAT(25) DEFAULT '0',
  [al_printed] char(1) DEFAULT 'N',
  PRIMARY KEY ([al_id]),
) ;
BULK INSERT [aplink] FROM 'E:\ETL Processing\Gateway\Import\Extract\aplink.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors10.txt');


-------------------------


SELECT 'apmanifest';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='apmanifest') DROP TABLE [apmanifest];
CREATE TABLE [apmanifest] (
  [am_id] int NOT NULL,
  [am_sending_id] char(16) DEFAULT '',
  [am_receiving_id] char(16) DEFAULT '',
  [am_submitted] datetime default NULL,
  [am_lodged] datetime default NULL,
  [am_created] datetime default NULL,
  [am_connotes] int DEFAULT '0',
  [am_labels] int DEFAULT '0',
  [am_transmitted] datetime default NULL,
  [am_local_id] int DEFAULT '0',
  [am_received] datetime default NULL,

  [am_approved] datetime default NULL,

  [am_cancelled] datetime default NULL,
  PRIMARY KEY ([am_id])
) ;
BULK INSERT [apmanifest] FROM 'E:\ETL Processing\Gateway\Import\Extract\apmanifest.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors11.txt');

-------------------------


SELECT 'authinfo';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='authinfo') DROP TABLE [authinfo];
CREATE TABLE [authinfo] (
  [ai_id] int NOT NULL,
  [ai_type] char(1) NOT NULL DEFAULT '',
  [ai_name] varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY ([ai_id])
) ;
BULK INSERT [authinfo] FROM 'E:\ETL Processing\Gateway\Import\Extract\authinfo.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors12.txt');


-------------------------

SELECT 'billing';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='billing') DROP TABLE [billing];
CREATE TABLE [billing] (
  [b_id] int NOT NULL,
  [b_company] int NOT NULL DEFAULT '0',
  [b_date] SMALLDATETIME default NULL,
  [b_stamp] datetime default NULL,
  [b_consignments] int DEFAULT '0',
  [b_bookins] int DEFAULT '0',
  [b_total] FLOAT(25) DEFAULT '0',
  PRIMARY KEY ([b_id])
) ;
BULK INSERT [billing] FROM 'E:\ETL Processing\Gateway\Import\Extract\billing.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors13.txt');


-------------------------


SELECT 'bookins';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='bookins') DROP TABLE [bookins];
CREATE TABLE [bookins] (
  [bi_id] int NOT NULL,
  [bi_company_id] int NOT NULL DEFAULT '0',
  [bi_import_id] int DEFAULT NULL,
  [bi_warehouse_id] int NOT NULL DEFAULT '0',
  [bi_date] SMALLDATETIME default NULL,
  [bi_connote] varchar(32) NOT NULL DEFAULT '',
  [bi_charge] varchar(32) NOT NULL DEFAULT '',
  [bi_units] int DEFAULT '1',
  [bi_units_entries] int DEFAULT '0',
  [bi_deadweight] FLOAT(25) DEFAULT NULL,
  [bi_volume] FLOAT(25) DEFAULT NULL,
  [bi_reference] varchar(64) DEFAULT '',
  [bi_sender_addr0] varchar(40) DEFAULT '',
  [bi_sender_addr1] varchar(40) DEFAULT '',
  [bi_sender_addr2] varchar(40) DEFAULT '',
  [bi_sender_addr3] varchar(40) DEFAULT '',
  [bi_sender_suburb] varchar(30) DEFAULT '',
  [bi_sender_postcode] int DEFAULT '0',
  [bi_receiver_addr0] varchar(40) DEFAULT '',
  [bi_receiver_addr1] varchar(40) DEFAULT '',
  [bi_receiver_addr2] varchar(40) DEFAULT '',
  [bi_receiver_addr3] varchar(40) DEFAULT '',
  [bi_receiver_suburb] varchar(30) DEFAULT '',
  [bi_receiver_postcode] int DEFAULT '0',
  [bi_pricecode] varchar(16) DEFAULT 'BOOKIN',
  [bi_special] varchar(80) DEFAULT '',
  [bi_not_before] SMALLDATETIME default NULL,
  [bi_not_after] SMALLDATETIME default NULL,
  [bi_revised_date] SMALLDATETIME default NULL,
  [bi_revised_time] time DEFAULT '00:00:00',
  [bi_received] SMALLDATETIME default NULL,
  [bi_released] SMALLDATETIME default NULL,
  [bi_comments] varchar(80) DEFAULT NULL,
  [bi_label] varchar(64) DEFAULT NULL,
  [bi_scan_received] datetime default NULL,
  [bi_scan_released] datetime default NULL,
  [bi_billing_id] int DEFAULT '0',
  [bi_billing_date] SMALLDATETIME default NULL,
  [bi_export_id] int DEFAULT '0',
  [bi_export2_id] int DEFAULT '0',
  [bi_agent_pod_name] varchar(64) DEFAULT NULL,
  [bi_agent_pod_stamp] datetime default NULL,
  [bi_agent_pod_entry] datetime default NULL,
  PRIMARY KEY ([bi_id]),
) ;
BULK INSERT [bookins] FROM 'E:\ETL Processing\Gateway\Import\Extract\bookins.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors14.txt');
-------------------------



SELECT 'bookintrack';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='bookintrack') DROP TABLE [bookintrack];
CREATE TABLE [bookintrack] (
  [bt_id] int NOT NULL,
  [bt_bookin] int NOT NULL DEFAULT '0',
  PRIMARY KEY ([bt_id]),
) ;
BULK INSERT [bookintrack] FROM 'E:\ETL Processing\Gateway\Import\Extract\bookintrack.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors15.txt');

-------------------------


SELECT 'branchs';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='branchs') DROP TABLE [branchs];
CREATE TABLE [branchs] (
  [b_id] int NOT NULL,
  [b_name] varchar(32) NOT NULL DEFAULT '',
  [b_trackhost] varchar(128) NOT NULL DEFAULT '',
  [b_trackport] varchar(32) NOT NULL DEFAULT '',
  [b_multitrack] char(1) DEFAULT 'N',
  [b_depot] int DEFAULT NULL,
  [b_shortname] varchar(8) DEFAULT '',
  [b_internal_name] varchar(128) DEFAULT NULL,
  [b_emmcode] varchar(64) DEFAULT NULL,
  [b_in_difot] char(1) DEFAULT 'N',
  [b_description_metro] char(128) DEFAULT '',
  [b_description_country] char(128) DEFAULT '',
  PRIMARY KEY ([b_id])
) ;
BULK INSERT [branchs] FROM 'E:\ETL Processing\Gateway\Import\Extract\branchs.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors16.txt');

-------------------------


SELECT 'cdaudit';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdaudit') DROP TABLE [cdaudit];
CREATE TABLE [cdaudit] (
  [ca_id] int NOT NULL,
  [ca_consignment] int NOT NULL DEFAULT '0',
  [ca_stamp] datetime default NULL,
  [ca_account] varchar(32) DEFAULT '',
  [ca_info] varchar(255) DEFAULT '',
  PRIMARY KEY ([ca_id]),
) ;
BULK INSERT [cdaudit] FROM 'E:\ETL Processing\Gateway\Import\Extract\cdaudit.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors17.txt');


-------------------------
 

SELECT 'cdcosmosbooking';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdcosmosbooking') DROP TABLE [cdcosmosbooking];
CREATE TABLE [cdcosmosbooking] (
  [cb_consignment] int NOT NULL DEFAULT '0',
  [cb_send_stamp] datetime default NULL,
  [cb_reply_stamp] datetime default NULL,
  [cb_branch] int DEFAULT '0',
  [cb_cosmos_date] SMALLDATETIME default NULL,
  [cb_cosmos_job] int DEFAULT '0',
  [cb_style] char(1) DEFAULT 'C',
  [cb_card_notified] char(1) DEFAULT 'N',
  [cb_sms_stamp] datetime default NULL,
  [cb_sms_number] char(16) DEFAULT '',
  [cb_reschedule_stamp] datetime default NULL,
  [cb_reschedule_date] SMALLDATETIME default NULL,
  [cb_reschedule_am_pm] char(1) DEFAULT '',
  [cb_reschedule_style] char(1) DEFAULT '',
  [cb_reschedule_instructions] char(80) DEFAULT '',
  [cb_redelivery_stamp] datetime default NULL,
  [cb_redelivery_date] SMALLDATETIME default NULL,
  [cb_redelivery_am_pm] char(1) DEFAULT '',
  [cb_redelivery_style] char(1) DEFAULT '',
  [cb_redelivery_instructions] char(80) DEFAULT '',
  PRIMARY KEY ([cb_consignment])
) ;
BULK INSERT [cdcosmosbooking] FROM 'E:\ETL Processing\Gateway\Import\Extract\cdcosmosbooking.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors18.txt');


-------------------------


SELECT 'cdcoupon';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdcoupon') DROP TABLE [cdcoupon];
CREATE TABLE [cdcoupon] (
  [cc_id] int NOT NULL,
  [cc_company_id] int NOT NULL DEFAULT '0',
  [cc_consignment] int NOT NULL DEFAULT '0',
  [cc_coupon] char(32) DEFAULT '',
  [cc_activity_stamp] datetime default NULL,
  [cc_pickup_stamp] datetime default NULL,
  [cc_accept_stamp] datetime default NULL,
  [cc_indepot_stamp] datetime default NULL,
  [cc_transfer_stamp] datetime default NULL,
  [cc_deliver_stamp] datetime default NULL,
  [cc_failed_stamp] datetime default NULL,
  [cc_activity_driver] int DEFAULT '0',
  [cc_pickup_driver] int DEFAULT '0',
  [cc_accept_driver] int DEFAULT '0',
  [cc_indepot_driver] int DEFAULT '0',
  [cc_transfer_driver] int DEFAULT '0',
  [cc_transfer_to] int DEFAULT '0',
  [cc_toagent_driver] int DEFAULT '0',
  [cc_toagent_stamp] datetime default NULL,
  [cc_toagent_name] char(64) DEFAULT '',
  [cc_deliver_driver] int DEFAULT '0',
  [cc_failed_driver] int DEFAULT '0',
  [cc_deliver_pod] char(64) DEFAULT '',
  [cc_failed] char(1) DEFAULT 'N',
  [cc_exception_stamp] datetime default NULL,
  [cc_exception_code] char(8) DEFAULT '',
  [cc_unit_type] char(8) DEFAULT '',
  [cc_internal] char(1) DEFAULT 'N',
  [cc_link_coupon] char(32) DEFAULT '',
  [cc_dirty] char(1) DEFAULT 'Y',
  [cc_last_status] char(64) DEFAULT '',
  [cc_last_driver] int DEFAULT '0',
  [cc_last_stamp] datetime default NULL,
  [cc_last_info] char(32) DEFAULT NULL,
  [cc_accept_driver_branch] int DEFAULT NULL,
  [cc_activity_driver_branch] int DEFAULT NULL,
  [cc_deliver_driver_branch] int DEFAULT NULL,
  [cc_failed_driver_branch] int DEFAULT NULL,
  [cc_indepot_driver_branch] int DEFAULT NULL,
  [cc_last_driver_branch] int DEFAULT NULL,
  [cc_pickup_driver_branch] int DEFAULT NULL,
  [cc_toagent_driver_branch] int DEFAULT NULL,
  [cc_tranfer_driver_branch] int DEFAULT NULL,
  PRIMARY KEY ([cc_id]),
) ;
BULK INSERT [cdcoupon] FROM 'E:\ETL Processing\Gateway\Import\Extract\cdcoupon.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors19.txt');



-------------------------

SELECT 'cdinternal';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdinternal') DROP TABLE [cdinternal];
CREATE TABLE [cdinternal] (
  [ci_id] int NOT NULL,
  [ci_company_id] int NOT NULL DEFAULT '0',
  [ci_consignment] int NOT NULL DEFAULT '0',
  [ci_parent] int NOT NULL DEFAULT '0',
  [ci_coupon] char(32) DEFAULT '',
  PRIMARY KEY ([ci_id]),
) ;
BULK INSERT [cdinternal] FROM 'E:\ETL Processing\Gateway\Import\Extract\cdinternal.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors20.txt');


-------------------------


SELECT 'cdref';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdref') DROP TABLE [cdref];
CREATE TABLE [cdref] (
  [cr_id] int NOT NULL,
  [cr_company_id] int NOT NULL DEFAULT '0',
  [cr_consignment] int NOT NULL DEFAULT '0',
  [cr_reference] char(32) DEFAULT '',
  PRIMARY KEY ([cr_id]),
) ;
BULK INSERT [cdref] FROM 'E:\ETL Processing\Gateway\Import\Extract\cdref.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors21.txt');



-------------------------

SELECT 'cdsundry';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cdsundry') DROP TABLE [cdsundry];
CREATE TABLE [cdsundry] (
  [cn_consignment] int NOT NULL DEFAULT '0',
  [cn_status] varchar(8) DEFAULT '',
  [cn_cost] FLOAT(25) DEFAULT NULL,
  [cn_note] text,
  [cn_stamp] datetime default NULL,
  [cn_notified] char(1) DEFAULT 'N',
  [cn_delcared_value] FLOAT(25) DEFAULT NULL,
  [cn_insurance] varchar(8) DEFAULT '',
  [cn_dangerous_goods] char(1) DEFAULT 'N',
  [cn_email] varchar(255) DEFAULT '',
  [cn_validation_info] varchar(255) DEFAULT '',
  [cn_releaseasn] char(25) DEFAULT '',
  [cn_notbefore] SMALLDATETIME default NULL,
  [cn_notafter] SMALLDATETIME default NULL,
  [cn_ezyfreight_price] FLOAT(25) DEFAULT '0',
  PRIMARY KEY ([cn_consignment])
) ;
BULK INSERT [cdsundry] FROM 'E:\ETL Processing\Gateway\Import\Extract\cdsundry.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors22.txt');



-------------------------

SELECT 'checkwork';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='checkwork') DROP TABLE [checkwork];
CREATE TABLE [checkwork] (
  [zoning_id] int NOT NULL DEFAULT '0',
  [postcode] int DEFAULT NULL,
  [suburb] varchar(64) DEFAULT NULL,
  [state] varchar(10) DEFAULT NULL,
  [agent_id] int NOT NULL DEFAULT '0',
  [zoneid] int DEFAULT '0',
  [subzoneid] int DEFAULT '0',
  [branch] int DEFAULT NULL,
  [depot] int DEFAULT NULL,
  [cppldriver] int DEFAULT '0',
  [ai_id] int NOT NULL,
  [ai_eta_zone] varchar(12) DEFAULT '',
  [ai_sort_code] varchar(12) DEFAULT NULL,
  [a_imported] char(1) DEFAULT 'N',
  [ai_sort_nsw] char(1) DEFAULT '',
  [ai_sort_act] char(1) DEFAULT '',
  [ai_sort_vic] char(1) DEFAULT '',
  [ai_sort_tas] char(1) DEFAULT '',
  [ai_sort_qld] char(1) DEFAULT '',
  [ai_sort_sa] char(1) DEFAULT '',
  [ai_sort_nt] char(1) DEFAULT '',
  [ai_sort_wa] char(1) DEFAULT '',
  [ai_sort_suffix] char(8) DEFAULT '',
  PRIMARY KEY ([ai_id]),
) ;
BULK INSERT [checkwork] FROM 'E:\ETL Processing\Gateway\Import\Extract\checkwork.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors23.txt');


-------------------------

SELECT 'chrisco';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='chrisco') DROP TABLE [chrisco];
CREATE TABLE [chrisco] (
  [ch_id] int NOT NULL,
  [ch_stamp] datetime default NULL,
  [ch_barcode] varchar(64) NOT NULL DEFAULT '',
  [ch_code] int DEFAULT '1',
  [ch_podstamp] datetime default NULL,
  [ch_podname] varchar(255) DEFAULT '',
  [ch_location] varchar(8) DEFAULT 'SYD',
  [ch_processstamp] datetime default NULL,
  [ch_success] char(1) DEFAULT 'N',
  PRIMARY KEY ([ch_id])
) ;
BULK INSERT [chrisco] FROM 'E:\ETL Processing\Gateway\Import\Extract\chrisco.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors24.txt');

-------------------------


SELECT 'companies';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companies') DROP TABLE [companies];
CREATE TABLE [companies] (
  [c_id] int  NOT NULL,
  [c_code] varchar(8) NOT NULL DEFAULT '',
  [c_name] varchar(32) DEFAULT NULL,
  [c_zoning_id] int NOT NULL DEFAULT '0',
  [c_pricegroup_id] int NOT NULL DEFAULT '0',
  [c_class] int NOT NULL DEFAULT '1',
  [c_returns] char(1) DEFAULT 'N',
  [c_return_connote_format] varchar(20) NOT NULL DEFAULT '',
  [c_return_tracking_format] varchar(20) NOT NULL DEFAULT '',
  [c_return_manifest_format] varchar(20) NOT NULL DEFAULT '',
  [c_return_connote_id] int DEFAULT NULL,
  [c_return_tracking_id] int DEFAULT NULL,
  [c_return_manifest_id] int DEFAULT '1',
  [c_default_addr0] varchar(40) DEFAULT NULL,
  [c_default_addr1] varchar(40) DEFAULT NULL,
  [c_default_addr2] varchar(40) DEFAULT NULL,
  [c_default_addr3] varchar(40) DEFAULT NULL,
  [c_default_suburb] varchar(30) DEFAULT NULL,
  [c_default_postcode] int DEFAULT NULL,
  [c_podrequired] char(1) DEFAULT 'Y',
  [c_active] char(1) DEFAULT 'N',
  [c_update_style] varchar(16) DEFAULT NULL,
  [c_autoquery] char(1) DEFAULT 'N',
  [c_billing_cycle] char(1) DEFAULT 'W',
  [c_billing_trigger] char(1) DEFAULT 'D',
  [c_billing_group] varchar(16) DEFAULT NULL,
  [c_abn] varchar(16) DEFAULT NULL,
  [c_gstreg] varchar(16) DEFAULT NULL,
  [c_asic] varchar(5) DEFAULT NULL,
  [c_salesrep_id] int DEFAULT NULL,
  [c_billing_name] varchar(40) DEFAULT NULL,
  [c_billing_addr0] varchar(40) DEFAULT NULL,
  [c_billing_addr1] varchar(40) DEFAULT NULL,
  [c_billing_addr2] varchar(40) DEFAULT NULL,
  [c_billing_addr3] varchar(40) DEFAULT NULL,
  [c_billing_suburb] varchar(30) DEFAULT NULL,
  [c_billing_postcode] int DEFAULT NULL,
  [c_billing_email] varchar(255) DEFAULT NULL,
  [c_default_paystyle] char(1) DEFAULT 'Q',
  [c_default_bank] varchar(32) DEFAULT NULL,
  [c_default_branch] varchar(32) DEFAULT NULL,
  [c_default_drawer] varchar(50) DEFAULT NULL,
  [c_cosmos_transfer] char(1) DEFAULT 'N',
  [c_auth_owner] varchar(32) DEFAULT '',
  [c_auth_group] varchar(32) DEFAULT '',
  [c_validation_id] int DEFAULT NULL,
  [c_ezyfreight_pricecode] varchar(16) DEFAULT '48',
  [c_ezyfreight_volume] FLOAT(25) DEFAULT '0',
  [c_ezyfreight_weight] int DEFAULT '0',
  [c_ezyfreight_pieces] char(1) DEFAULT 'N',
  [c_ezyfreight_zonemap] int DEFAULT '0',
  [c_ezyfreight_connote_format] varchar(20) DEFAULT 'CP{ID}E%07d',
  [c_ezyfreight_connote_id] int DEFAULT '1',
  [c_ezyfreight_consolidate] char(1) DEFAULT 'N',
  [c_ezyfreight_insurance] char(1) DEFAULT '0',
  [c_ezyfreight_insurance_choices] char(1) DEFAULT 'N',
  [c_returns_pricecode] varchar(16) DEFAULT '48',
  [c_last_check] int DEFAULT '0',
  [c_check_frequency] int DEFAULT '30',
  [c_cosmos_code] varchar(16) DEFAULT '',
  [c_update_format] char(1) DEFAULT 'N',
  [c_update_frequency] char(1) DEFAULT 'D',
  [c_update_ftp_host] varchar(64) DEFAULT '',
  [c_update_ftp_port] int DEFAULT '21',
  [c_update_ftp_user] varchar(64) DEFAULT '',
  [c_update_ftp_password] varchar(64) DEFAULT '',
  [c_update_ftp_directory] varchar(64) DEFAULT '/',
  [c_update_email] varchar(255) DEFAULT '',
  [c_update_filename_format] varchar(64) DEFAULT 'CPPL%05d.csv',
  [c_update_id] int DEFAULT '1',
  [c_update_passive] char(1) DEFAULT 'N',
  [c_export_special] char(1) DEFAULT 'N',
  [c_cubic_factor] FLOAT(25) DEFAULT '0',
  [c_difot_failedok] char(1) DEFAULT 'N',
  [c_application_details] varchar(64) DEFAULT '',
  [c_notes] text,
  [c_ezyfreight_default_instructions] varchar(64) DEFAULT '',
  [c_ezyfreight_no_auto_release] char(1) DEFAULT 'N',
  [c_ezyfreight_cube_exempt] char(1) DEFAULT 'N',
  [c_daily_deliveries_email] varchar(255) DEFAULT '',
  [c_sms_on_pickup] char(1) DEFAULT 'N',
  [c_alternate_agents] char(1) DEFAULT 'N',
  [c_items_and_coupons] char(1) DEFAULT 'N',
  [c_no_oversize_warning] char(1) DEFAULT 'N',
  PRIMARY KEY ([c_id])
) ;
BULK INSERT [companies] FROM 'E:\ETL Processing\Gateway\Import\Extract\companies.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors25.txt');


-------------------------

SELECT 'companyaccount';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyaccount') DROP TABLE [companyaccount];
CREATE TABLE [companyaccount] (
  [ca_company_id] int NOT NULL DEFAULT '0',
  [ca_order] int NOT NULL DEFAULT '0',
  [ca_match] varchar(32) NOT NULL DEFAULT '',
  [ca_account] varchar(32) NOT NULL DEFAULT '',
) ;
BULK INSERT [companyaccount] FROM 'E:\ETL Processing\Gateway\Import\Extract\companyaccount.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors26.txt');


-------------------------

SELECT 'companyagentoverride';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyagentoverride') DROP TABLE [companyagentoverride];
CREATE TABLE [companyagentoverride] (
  [ao_company_id] int NOT NULL DEFAULT '0',
  [ao_agent_id] int NOT NULL DEFAULT '0',
) ;
BULK INSERT [companyagentoverride] FROM 'E:\ETL Processing\Gateway\Import\Extract\companyagentoverride.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors.txt');

-------------------------


SELECT 'companyclass';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyclass') DROP TABLE [companyclass];
CREATE TABLE [companyclass] (
  [cc_id] int NOT NULL,
  [cc_shortname] varchar(32) NOT NULL DEFAULT '',
  [cc_longname] varchar(64) NOT NULL DEFAULT '',
  [cc_pickup_portion] varchar(64) NOT NULL DEFAULT '',
  [cc_delivery_portion] varchar(64) NOT NULL DEFAULT '',
  [cc_export_prefix] varchar(64) DEFAULT '',
  [cc_export_date] SMALLDATETIME default NULL,
  [cc_export_id] int DEFAULT '1',
  [cc_export_required] char(1) DEFAULT 'N',
  [cc_export_ftp_host] varchar(128) DEFAULT NULL,
  [cc_export_ftp_user] varchar(128) DEFAULT NULL,
  [cc_export_ftp_password] varchar(128) DEFAULT NULL,
  [cc_export_ftp_directory] varchar(128) DEFAULT NULL,
  [cc_ogm] char(1) DEFAULT 'N',
  [cc_direct_book_ezyfreight] char(1) DEFAULT 'N',
  [cc_export2_ftp_host] varchar(128) DEFAULT NULL,
  [cc_export2_ftp_user] varchar(128) DEFAULT NULL,
  [cc_export2_ftp_password] varchar(128) DEFAULT NULL,
  [cc_export2_ftp_directory] varchar(128) DEFAULT NULL,
  [cc_export2_required] char(1) DEFAULT 'N',
  [cc_export2_date] SMALLDATETIME default NULL,
  [cc_export2_id] int DEFAULT '1',
  [cc_export2_prefix] varchar(64) DEFAULT '',
  [cc_export2_daily] char(1) DEFAULT 'N',
  [cc_pay_prefix] varchar(16) DEFAULT '',
  [cc_base_prefix] varchar(8) DEFAULT '',
  [cc_label_name] varchar(255) DEFAULT '',
  [cc_image_file] varchar(255) DEFAULT 'image-POD.ps',
  [cc_invoice_reprint] char(1) DEFAULT 'N',
  PRIMARY KEY ([cc_id])
) ;
BULK INSERT [companyclass] FROM 'E:\ETL Processing\Gateway\Import\Extract\companyclass.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors27.txt');

-------------------------


SELECT 'companyclpricecode';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyclpricecode') DROP TABLE [companyclpricecode];
CREATE TABLE [companyclpricecode] (
  [clp_company_id] int NOT NULL DEFAULT '0',
  [clp_order] int NOT NULL DEFAULT '0',
  [clp_pricecode] varchar(32) NOT NULL DEFAULT '',
) ;
BULK INSERT [companyclpricecode] FROM 'E:\ETL Processing\Gateway\Import\Extract\companyclpricecode.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors.txt');


-------------------------

SELECT 'companycontact';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companycontact') DROP TABLE [companycontact];
CREATE TABLE [companycontact] (
  [cc_company_id] int NOT NULL DEFAULT '0',
  [cc_order] int NOT NULL DEFAULT '0',
  [cc_style] varchar(20) DEFAULT NULL,
  [cc_name] varchar(40) DEFAULT NULL,
  [cc_phone] varchar(16) DEFAULT NULL,
  [cc_email] varchar(255) DEFAULT NULL,
) ;
BULK INSERT [companycontact] FROM 'E:\ETL Processing\Gateway\Import\Extract\companycontact.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors28.txt');

-------------------------


SELECT 'companyefpricecode';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyefpricecode') DROP TABLE [companyefpricecode];
CREATE TABLE [companyefpricecode] (
  [cep_company_id] int NOT NULL DEFAULT '0',
  [cep_order] int NOT NULL DEFAULT '0',
  [cep_pricecode] varchar(32) NOT NULL DEFAULT '',
) ;
BULK INSERT [companyefpricecode] FROM 'E:\ETL Processing\Gateway\Import\Extract\companyefpricecode.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors.txt');

-------------------------


SELECT 'companypricecode';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companypricecode') DROP TABLE [companypricecode];
CREATE TABLE [companypricecode] (
  [cp_company_id] int NOT NULL DEFAULT '0',
  [cp_order] int NOT NULL DEFAULT '0',
  [cp_match] varchar(32) NOT NULL DEFAULT '',
  [cp_pricecode] varchar(32) NOT NULL DEFAULT '',
  [cp_cubic_factor] char(1) DEFAULT 'N',
  [cp_style] char(1) DEFAULT 'I',
) ;
BULK INSERT [companypricecode] FROM 'E:\ETL Processing\Gateway\Import\Extract\companypricecode.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors29.txt');


-------------------------

SELECT 'companyremapagent';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companyremapagent') DROP TABLE [companyremapagent];
CREATE TABLE [companyremapagent] (
  [ra_company_id] int NOT NULL DEFAULT '0',
  [ra_order] int NOT NULL DEFAULT '0',
  [ra_from_agent_id] int NOT NULL DEFAULT '0',
  [ra_to_agent_id] int NOT NULL DEFAULT '0',
  [ra_pickup] char(1) NOT NULL DEFAULT 'N',
  [ra_delivery] char(1) NOT NULL DEFAULT 'N',
  [ra_import_only] char(1) DEFAULT 'N',
) ;
BULK INSERT [companyremapagent] FROM 'E:\ETL Processing\Gateway\Import\Extract\companyremapagent.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors30.txt');


-------------------------

SELECT 'companysscc';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='companysscc') DROP TABLE [companysscc];
CREATE TABLE [companysscc] (
  [cs_company_id] int NOT NULL DEFAULT '0',
  [cs_order] int NOT NULL DEFAULT '0',
  [cs_match] varchar(32) NOT NULL DEFAULT '',
) ;
BULK INSERT [companysscc] FROM 'E:\ETL Processing\Gateway\Import\Extract\companysscc.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors31.txt');


-------------------------

SELECT 'consignment';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='consignment') DROP TABLE [consignment];
CREATE TABLE [consignment] (
   [cd_id] int NOT NULL,
  [cd_company_id] int NOT NULL DEFAULT '0',
  [cd_account] varchar(32) DEFAULT '',
  [cd_agent_id] int DEFAULT NULL,
  [cd_import_id] int DEFAULT NULL,
  [cd_ogm_id] int DEFAULT '0',
  [cd_manifest_id] int DEFAULT '0',
  [cd_connote] varchar(32) NOT NULL DEFAULT '',
  [cd_date] SMALLDATETIME default NULL,
  [cd_consignment_date] SMALLDATETIME default NULL,
  [cd_eta_date] SMALLDATETIME default NULL,
  [cd_eta_earliest] SMALLDATETIME default NULL,
  [cd_customer_eta] SMALLDATETIME default NULL,
  [cd_pickup_addr0] varchar(40) DEFAULT '',
  [cd_pickup_addr1] varchar(40) DEFAULT '',
  [cd_pickup_addr2] varchar(40) DEFAULT '',
  [cd_pickup_addr3] varchar(40) DEFAULT '',
  [cd_pickup_suburb] varchar(30) DEFAULT '',
  [cd_pickup_postcode] int DEFAULT '0',
  [cd_pickup_record_no] int DEFAULT '0',
  [cd_pickup_confidence] int DEFAULT '0',
  [cd_pickup_contact] varchar(40) DEFAULT '',
  [cd_pickup_contact_phone] varchar(20) DEFAULT '',
  [cd_delivery_addr0] varchar(40) DEFAULT '',
  [cd_delivery_addr1] varchar(40) DEFAULT '',
  [cd_delivery_addr2] varchar(40) DEFAULT '',
  [cd_delivery_addr3] varchar(40) DEFAULT '',
  [cd_delivery_email] varchar(255) DEFAULT '',
  [cd_delivery_suburb] varchar(30) DEFAULT '',
  [cd_delivery_postcode] int DEFAULT NULL,
  [cd_delivery_record_no] int DEFAULT '0',
  [cd_delivery_confidence] int DEFAULT '0',
  [cd_delivery_contact] varchar(40) DEFAULT '',
  [cd_delivery_contact_phone] varchar(20) DEFAULT '',
  [cd_special_instructions] varchar(64) DEFAULT NULL,
  [cd_stats_branch] int DEFAULT NULL,
  [cd_stats_depot] int DEFAULT NULL,
  [cd_pickup_branch] int DEFAULT NULL,
  [cd_pickup_pay_branch] int DEFAULT NULL,
  [cd_deliver_branch] int DEFAULT NULL,
  [cd_deliver_pay_branch] int DEFAULT NULL,
  [cd_special_driver] int DEFAULT '0',
  [cd_pickup_revenue] FLOAT(25) DEFAULT '0',
  [cd_deliver_revenue] FLOAT(25) DEFAULT '0',
  [cd_pickup_billing] FLOAT(25) DEFAULT '0',
  [cd_deliver_billing] FLOAT(25) DEFAULT '0',
  [cd_pickup_charge] FLOAT(25) DEFAULT '0',
  [cd_pickup_charge_actual] FLOAT(25) DEFAULT '0',
  [cd_deliver_charge] FLOAT(25) DEFAULT '0',
  [cd_deliver_payment_actual] FLOAT(25) DEFAULT '0',
  [cd_pickup_payment] FLOAT(25) DEFAULT '0',
  [cd_pickup_payment_actual] FLOAT(25) DEFAULT '0',
  [cd_deliver_payment] FLOAT(25) DEFAULT '0',
  [cd_deliver_charge_actual] FLOAT(25) DEFAULT '0',
  [cd_special_payment] FLOAT(25) DEFAULT '0',
  [cd_insurance_billing] FLOAT(25) DEFAULT '0',
  [cd_items] int DEFAULT NULL,
  [cd_coupons] int DEFAULT NULL,
  [cd_references] int DEFAULT NULL,
  [cd_rating_id] int DEFAULT NULL,
  [cd_chargeunits] int DEFAULT NULL,
  [cd_deadweight] FLOAT(25) DEFAULT NULL,
  [cd_dimension0] FLOAT(25) DEFAULT NULL,
  [cd_dimension1] FLOAT(25) DEFAULT NULL,
  [cd_dimension2] FLOAT(25) DEFAULT NULL,
  [cd_volume] FLOAT(25) DEFAULT NULL,
  [cd_volume_automatic] char(1) DEFAULT 'N',
  [cd_import_deadweight] FLOAT(25) DEFAULT NULL,
  [cd_import_volume] FLOAT(25) DEFAULT NULL,
  [cd_measured_deadweight] FLOAT(25) DEFAULT NULL,
  [cd_measured_volume] FLOAT(25) DEFAULT NULL,
  [cd_billing_id] int DEFAULT '0',
  [cd_billing_date] SMALLDATETIME default NULL,
  [cd_export_id] int DEFAULT '0',
  [cd_export2_id] int DEFAULT '0',
  [cd_pickup_pay_date] SMALLDATETIME default NULL,
  [cd_delivery_pay_date] SMALLDATETIME default NULL,
  [cd_transfer_pay_date] SMALLDATETIME default NULL,
  [cd_activity_stamp] datetime default NULL,
  [cd_activity_driver] int DEFAULT '0',
  [cd_pickup_stamp] datetime default NULL,
  [cd_pickup_driver] int DEFAULT '0',
  [cd_pickup_pay_driver] int DEFAULT '0',
  [cd_pickup_count] int DEFAULT '0',
  [cd_pickup_notified] char(1) DEFAULT 'N',
  [cd_accept_stamp] datetime default NULL,
  [cd_accept_driver] int DEFAULT '0',
  [cd_accept_count] int DEFAULT '0',
  [cd_accept_notified] char(1) DEFAULT 'N',
  [cd_indepot_notified] char(1) DEFAULT 'N',
  [cd_indepot_stamp] datetime default NULL,
  [cd_indepot_driver] int DEFAULT '0',
  [cd_indepot_count] int DEFAULT '0',
  [cd_transfer_notified] char(1) DEFAULT 'N',
  [cd_failed_stamp] datetime default NULL,
  [cd_failed_driver] int DEFAULT '0',
  [cd_deliver_stamp] datetime default NULL,
  [cd_deliver_driver] int DEFAULT '0',
  [cd_deliver_pay_driver] int DEFAULT '0',
  [cd_deliver_count] int DEFAULT '0',
  [cd_deliver_pod] varchar(64) DEFAULT '',
  [cd_deliver_notified] char(1) DEFAULT 'N',
  [cd_pickup_pay_notified] char(1) DEFAULT 'N',
  [cd_deliver_pay_notified] char(1) DEFAULT 'N',
  [cd_printed] char(1) DEFAULT 'Y',
  [cd_returns] char(1) DEFAULT 'N',
  [cd_release] char(1) DEFAULT 'N',
  [cd_release_stamp] datetime default NULL,
  [cd_pricecode] varchar(16) DEFAULT 'STD',
  [cd_insurance] char(1) DEFAULT '0',
  [cd_pickup_agent] int DEFAULT '0',
  [cd_delivery_agent] int DEFAULT '0',
  [cd_agent_pod] char(1) DEFAULT 'N',
  [cd_agent_pod_desired] char(1) DEFAULT 'N',
  [cd_agent_pod_name] varchar(64) DEFAULT '',
  [cd_agent_pod_stamp] datetime default NULL,
  [cd_agent_pod_entry] datetime default NULL,
  [cd_completed] char(1) DEFAULT 'N',
  [cd_cancelled] char(1) DEFAULT 'N',
  [cd_cancelled_stamp] datetime default NULL,
  [cd_cancelled_by] varchar(32) DEFAULT '',
  [cd_test] char(1) DEFAULT 'N',
  [cd_dirty] char(1) DEFAULT 'Y',
  [cd_transfer_stamp] datetime default NULL,
  [cd_transfer_driver] int DEFAULT '0',
  [cd_transfer_count] int DEFAULT '0',
  [cd_transfer_to] int DEFAULT '0',
  [cd_toagent_notified] char(1) DEFAULT 'N',
  [cd_toagent_stamp] datetime default NULL,
  [cd_toagent_driver] int DEFAULT '0',
  [cd_toagent_count] int DEFAULT '0',
  [cd_toagent_name] varchar(64) DEFAULT '',
  [cd_last_status] varchar(64) DEFAULT '',
  [cd_last_notified] char(1) DEFAULT 'N',
  [cd_last_info] varchar(32) DEFAULT NULL,
  [cd_last_driver] int DEFAULT '0',
  [cd_last_stamp] datetime default NULL,
  [cd_last_count] int DEFAULT '0',
  [cd_accept_driver_branch] int DEFAULT NULL,
  [cd_activity_driver_branch] int DEFAULT NULL,
  [cd_deliver_driver_branch] int DEFAULT NULL,
  [cd_deliver_pay_driver_branch] int DEFAULT NULL,
  [cd_failed_driver_branch] int DEFAULT NULL,
  [cd_indepot_driver_branch] int DEFAULT NULL,
  [cd_last_driver_branch] int DEFAULT NULL,
  [cd_pickup_driver_branch] int DEFAULT NULL,
  [cd_pickup_pay_driver_branch] int DEFAULT NULL,
  [cd_special_driver_branch] int DEFAULT NULL,
  [cd_toagent_driver_branch] int DEFAULT NULL,
  [cd_transfer_driver_branch] int DEFAULT NULL,
  PRIMARY KEY ([cd_id]),
) ;
BULK INSERT [consignment] FROM 'E:\ETL Processing\Gateway\Import\Extract\consignment.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors32.txt');

-------------------------


SELECT 'consignmentstatus';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='consignmentstatus') DROP TABLE [consignmentstatus];
CREATE TABLE [consignmentstatus] (
  [cs_code] varchar(8) NOT NULL DEFAULT '',
  [cs_description] varchar(64) NOT NULL DEFAULT '',
  [cs_dhl_code] varchar(8) DEFAULT NULL,
  [cs_failure] char(1) DEFAULT 'N',
  PRIMARY KEY ([cs_code])
) ;
BULK INSERT [consignmentstatus] FROM 'E:\ETL Processing\Gateway\Import\Extract\consignmentstatus.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors33.txt');
-------------------------



SELECT 'cpplcpngrid';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cpplcpngrid') DROP TABLE [cpplcpngrid];
CREATE TABLE [cpplcpngrid] (
  [ccg_pricegroup] int NOT NULL DEFAULT '0',
  [ccg_pricecode] varchar(16) NOT NULL DEFAULT '',
  [ccg_fromzone] int NOT NULL DEFAULT '0',
  [ccg_tozone] int NOT NULL DEFAULT '0',
  [ccg_present] char(1) DEFAULT 'N',
  [ccg_baseprice] FLOAT(25) DEFAULT '0',
  [ccg_15kg] FLOAT(25) DEFAULT '0',
  [ccg_25kg] FLOAT(25) DEFAULT '0',
  [ccg_pickup_payamount] FLOAT(25) DEFAULT '0',
  [ccg_pickup_15kg] FLOAT(25) DEFAULT '0',
  [ccg_pickup_25kg] FLOAT(25) DEFAULT '0',
  [ccg_deliver_payamount] FLOAT(25) DEFAULT '0',
  [ccg_deliver_15kg] FLOAT(25) DEFAULT '0',
  [ccg_deliver_25kg] FLOAT(25) DEFAULT '0',
) ;
BULK INSERT [cpplcpngrid] FROM 'E:\ETL Processing\Gateway\Import\Extract\cpplcpngrid.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors34.txt');

-------------------------


SELECT 'cpplgrid';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='cpplgrid') DROP TABLE [cpplgrid];
CREATE TABLE [cpplgrid] (
  [cg_pricegroup] int NOT NULL DEFAULT '0',
  [cg_pricecode] varchar(16) NOT NULL DEFAULT '',
  [cg_fromzone] int NOT NULL DEFAULT '0',
  [cg_tozone] int NOT NULL DEFAULT '0',
  [cg_present] char(1) DEFAULT 'N',
  [cg_baseprice] FLOAT(25) DEFAULT '0',
  [cg_kgincluded] FLOAT(25) DEFAULT '0',
  [cg_kgprice] FLOAT(25) DEFAULT '0',
  [cg_pickup_payamount] FLOAT(25) DEFAULT '0',
  [cg_pickup_paybreak] FLOAT(25) DEFAULT '25',
  [cg_pickup_kgpre] FLOAT(25) DEFAULT '0',
  [cg_pickup_kgpost] FLOAT(25) DEFAULT '0',
  [cg_deliver_payamount] FLOAT(25) DEFAULT '0',
  [cg_deliver_paybreak] FLOAT(25) DEFAULT '25',
  [cg_deliver_kgpre] FLOAT(25) DEFAULT '0',
  [cg_deliver_kgpost] FLOAT(25) DEFAULT '0',
) ;
BULK INSERT [cpplgrid] FROM 'E:\ETL Processing\Gateway\Import\Extract\cpplgrid.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors35.txt');


-------------------------

SELECT 'customereta';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='customereta') DROP TABLE [customereta];
CREATE TABLE [customereta] (
  [ce_from] char(12) DEFAULT '',
  [ce_to] char(12) DEFAULT '',
  [ce_days] FLOAT(25) DEFAULT '1',
) ;
BULK INSERT [customereta] FROM 'E:\ETL Processing\Gateway\Import\Extract\customereta.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors36.txt');




SELECT 'depot';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='depot') DROP TABLE [depot];
CREATE TABLE [depot] (
  [d_id] int NOT NULL,
  [d_code] varchar(16) DEFAULT NULL,
  [d_branch] int DEFAULT NULL,
  [d_name] varchar(64) DEFAULT NULL,
  [d_suburb] int DEFAULT NULL,
  [d_address0] varchar(32) DEFAULT NULL,
  [d_address1] varchar(32) DEFAULT NULL,
  [d_contact] varchar(32) DEFAULT NULL,
  [d_style] tinyint DEFAULT '3',
  PRIMARY KEY ([d_id]),
) ;
BULK INSERT [depot] FROM 'E:\ETL Processing\Gateway\Import\Extract\depot.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors37.txt');




SELECT 'dhlinfo';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dhlinfo') DROP TABLE [dhlinfo];
CREATE TABLE [dhlinfo] (
  [dhl_awb] varchar(16) NOT NULL DEFAULT '',
  [dhl_origin] varchar(4) DEFAULT NULL,
  [dhl_route] varchar(6) DEFAULT NULL,
  [dhl_service_area] varchar(4) DEFAULT NULL,
  [dhl_location] varchar(4) DEFAULT NULL,
  [dhl_userid] varchar(6) DEFAULT NULL,
  [dhl_completed] char(1) DEFAULT 'N',
  [dhl_consignment] int DEFAULT NULL,
  PRIMARY KEY ([dhl_awb]),
) ;
BULK INSERT [dhlinfo] FROM 'E:\ETL Processing\Gateway\Import\Extract\dhlinfo.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors38.txt');




SELECT 'dhlroute';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dhlroute') DROP TABLE [dhlroute];
CREATE TABLE [dhlroute] (
  [dr_route] char(8) NOT NULL,
  [dr_agent] int DEFAULT '0',
  [dr_pickup_suburb] char(64) DEFAULT NULL,
  [dr_pickup_postcode] int DEFAULT NULL,
  [dr_branch] int DEFAULT NULL,
  [dr_company_id] int DEFAULT '21',
  PRIMARY KEY ([dr_route])
) ;
BULK INSERT [dhlroute] FROM 'E:\ETL Processing\Gateway\Import\Extract\dhlroute.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors39.txt');




SELECT 'dirty_consignment';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dirty_consignment') DROP TABLE [dirty_consignment];
CREATE TABLE [dirty_consignment] (
  [dc_id] int NOT NULL DEFAULT '0',
  [dc_company_id] int NOT NULL DEFAULT '0',
  PRIMARY KEY ([dc_id])
) ;
BULK INSERT [dirty_consignment] FROM 'E:\ETL Processing\Gateway\Import\Extract\dirty_consignment.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors40.txt');




SELECT 'dirty_coupon';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='dirty_coupon') DROP TABLE [dirty_coupon];
CREATE TABLE [dirty_coupon] (
  [dc_id] int NOT NULL DEFAULT '0',
  [dc_company_id] int NOT NULL DEFAULT '0',
  PRIMARY KEY ([dc_id])
) ;
BULK INSERT [dirty_coupon] FROM 'E:\ETL Processing\Gateway\Import\Extract\dirty_coupon.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors41.txt');




SELECT 'driver';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='driver') DROP TABLE [driver];
CREATE TABLE [driver] (
  [dr_id] int NOT NULL,
  [dr_branch] int DEFAULT NULL,
  [dr_number] int DEFAULT NULL,
  [dr_name] char(64) DEFAULT NULL,
  [dr_depot] int DEFAULT NULL,
  [dr_warehouse] char(16) DEFAULT NULL,
  [dr_agentname] char(16) DEFAULT '',
  [dr_typecode] char(2) DEFAULT NULL,
  [dr_type] char(32) DEFAULT NULL,
  PRIMARY KEY ([dr_id]),
) ;
BULK INSERT [driver] FROM 'E:\ETL Processing\Gateway\Import\Extract\driver.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors42.txt');




SELECT 'ediconfig';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ediconfig') DROP TABLE [ediconfig];
CREATE TABLE [ediconfig] (
  [ec_parameter] varchar(256) NOT NULL,
  [ec_style] char(16) DEFAULT 'string',
  [ec_section] varchar(256) DEFAULT '',
  [ec_description] varchar(256) DEFAULT '',
  [ec_value] varchar(256) DEFAULT '',
) ;
BULK INSERT [ediconfig] FROM 'E:\ETL Processing\Gateway\Import\Extract\ediconfig.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors43.txt');




SELECT 'enquiry';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='enquiry') DROP TABLE [enquiry];
CREATE TABLE [enquiry] (
  [srno] int NOT NULL,
  [pname] varchar(100) NOT NULL DEFAULT '',
  [pcompanyno] varchar(100) NOT NULL DEFAULT '',
  [couponno] varchar(100) NOT NULL DEFAULT '',
  [type] varchar(100) NOT NULL DEFAULT '',
  [status] varchar(100) NOT NULL DEFAULT '',
  [sdatetime] datetime default NULL,
  [edatetime] datetime default NULL,
  [dhours] int NOT NULL DEFAULT '0',
  [dmin] int NOT NULL DEFAULT '0',
  [pdriver] int NOT NULL DEFAULT '0',
  [pdriverbranch] int DEFAULT '0',
  [contact] varchar(100) NOT NULL DEFAULT '',
  [padd1] varchar(100) NOT NULL DEFAULT '',
  [padd2] varchar(100) DEFAULT '',
  [padd3] varchar(100) DEFAULT '',
  [psub] varchar(100) NOT NULL DEFAULT '',
  [ppcode] varchar(10) DEFAULT '',
  [dadd1] varchar(100) NOT NULL DEFAULT '',
  [dadd2] varchar(100) DEFAULT '',
  [dadd3] varchar(100) DEFAULT '',
  [dsub] varchar(100) NOT NULL DEFAULT '',
  [dpcode] varchar(10) DEFAULT '',
  [dname] varchar(100) NOT NULL DEFAULT '',
  [dcompanyno] varchar(100) NOT NULL DEFAULT '',
  [ddriver] int NOT NULL DEFAULT '0',
  [ddriverbranch] int DEFAULT '0',
  [name] varchar(100) NOT NULL DEFAULT '',
  [branch] varchar(100) NOT NULL DEFAULT '',
  [callyn] varchar(100) NOT NULL DEFAULT '0',
  [lastcall] datetime default NULL,
  [goodd] varchar(100) NOT NULL DEFAULT '',
  [csr] varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY ([srno])
) ;
BULK INSERT [enquiry] FROM 'E:\ETL Processing\Gateway\Import\Extract\enquiry.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors43.txt');




SELECT 'enquiryd';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='enquiryd') DROP TABLE [enquiryd];
CREATE TABLE [enquiryd] (
  [srno] int NOT NULL,
  [date] SMALLDATETIME default NULL,
  [time] time NOT NULL DEFAULT '00:00:00',
  [csr] varchar(100) NOT NULL DEFAULT '',
  [details] text NOT NULL,
  [action] text NOT NULL,
  [ensrno] int NOT NULL DEFAULT '0',
  PRIMARY KEY ([srno])
) ;
BULK INSERT [enquiryd] FROM 'E:\ETL Processing\Gateway\Import\Extract\enquiryd.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors44.txt');




SELECT 'etagrid';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='etagrid') DROP TABLE [etagrid];
CREATE TABLE [etagrid] (
  [eg_from] varchar(10) NOT NULL DEFAULT '',
  [eg_to] varchar(10) NOT NULL DEFAULT '',
  [eg_postcode] int DEFAULT '0',
  [eg_suburb] varchar(40) DEFAULT '',
  [eg_send] tinyint NOT NULL DEFAULT '0',
  [eg_leg1] varchar(10) DEFAULT '',
  [eg_leg1_days] tinyint DEFAULT '0',
  [eg_leg2] varchar(10) DEFAULT '',
  [eg_leg2_days] tinyint DEFAULT '0',
  [eg_leg3] varchar(10) DEFAULT '',
  [eg_leg3_days] tinyint DEFAULT '0',
  [eg_to_days] tinyint DEFAULT '0',
) ;
BULK INSERT [etagrid] FROM 'E:\ETL Processing\Gateway\Import\Extract\etagrid.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors45.txt');




SELECT 'etastate';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='etastate') DROP TABLE [etastate];
CREATE TABLE [etastate] (
  [es_zone] char(8) DEFAULT NULL,
  [es_state] char(8) DEFAULT NULL,
) ;
BULK INSERT [etastate] FROM 'E:\ETL Processing\Gateway\Import\Extract\etastate.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors46.txt');




SELECT 'exceptionmap';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='exceptionmap') DROP TABLE [exceptionmap];
CREATE TABLE [exceptionmap] (
  [em_id] int NOT NULL,
  [em_style] char(1) DEFAULT 'D',
  [em_string] varchar(64) NOT NULL DEFAULT '',
  [em_status] varchar(8) NOT NULL DEFAULT '',
  [em_fullmatch] char(1) DEFAULT 'N',
  PRIMARY KEY ([em_id]),
) ;
BULK INSERT [exceptionmap] FROM 'E:\ETL Processing\Gateway\Import\Extract\exceptionmap.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors47.txt');




SELECT 'exports';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='exports') DROP TABLE [exports];
CREATE TABLE [exports] (
  [e_id] int NOT NULL,
  [e_stamp] datetime default NULL,
  [e_filename] varchar(64) NOT NULL DEFAULT '',
  [e_consignments] int DEFAULT '0',
  [e_bookins] int DEFAULT '0',
  [e_class] int DEFAULT '0',
  [e_2nd] char(1) DEFAULT 'N',
  PRIMARY KEY ([e_id])
) ;
BULK INSERT [exports] FROM 'E:\ETL Processing\Gateway\Import\Extract\exports.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors48.txt');




SELECT 'ftpsetup';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ftpsetup') DROP TABLE [ftpsetup];
CREATE TABLE [ftpsetup] (
  [fs_user] varchar(64) NOT NULL DEFAULT '',
  [fs_password] varchar(64) NOT NULL DEFAULT '',
  [fs_description] varchar(64) DEFAULT NULL,
  [fs_uid] int DEFAULT '32767',
  [fs_gid] int DEFAULT '32767',
  [fs_dir] varchar(255) DEFAULT '/tmp',
  PRIMARY KEY ([fs_user])
) ;
BULK INSERT [ftpsetup] FROM 'E:\ETL Processing\Gateway\Import\Extract\ftpsetup.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors49.txt');




SELECT 'goods';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='goods') DROP TABLE [goods];
CREATE TABLE [goods] (
  [g_id] int NOT NULL,
  [g_company_id] int DEFAULT '0',
  [g_user] varchar(32) DEFAULT '',
  [g_description] varchar(32) DEFAULT '',
  [g_deadweight] FLOAT(25) DEFAULT NULL,
  [g_dimension0] FLOAT(25) DEFAULT NULL,
  [g_dimension1] FLOAT(25) DEFAULT NULL,
  [g_dimension2] FLOAT(25) DEFAULT NULL,
  [g_volume] FLOAT(25) DEFAULT NULL,
  PRIMARY KEY ([g_id])
) ;
BULK INSERT [goods] FROM 'E:\ETL Processing\Gateway\Import\Extract\goods.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors50.txt');




SELECT 'groupname';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='groupname') DROP TABLE [groupname];
CREATE TABLE [groupname] (
  [groupname] varchar(32) NOT NULL DEFAULT '',
  [description] varchar(64) DEFAULT NULL,
  PRIMARY KEY ([groupname])
) ;
BULK INSERT [groupname] FROM 'E:\ETL Processing\Gateway\Import\Extract\groupname.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors51.txt');




SELECT 'groups';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='groups') DROP TABLE [groups];
CREATE TABLE [groups] (
  [account] varchar(32) NOT NULL DEFAULT '',
  [groups] varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY ([account],[groups])
) ;
BULK INSERT [groups] FROM 'E:\ETL Processing\Gateway\Import\Extract\groups.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors.txt52');




SELECT 'guessSSCC';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='guessSSCC') DROP TABLE [guessSSCC];
CREATE TABLE [guessSSCC] (
  [gs_portion] char(7) DEFAULT NULL,
  [gs_name] char(32) DEFAULT NULL,
  [gs_company_id] int DEFAULT NULL,
) ;
BULK INSERT [guessSSCC] FROM 'E:\ETL Processing\Gateway\Import\Extract\guessSSCC.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors53.txt');




SELECT 'holidays';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='holidays') DROP TABLE [holidays];
CREATE TABLE [holidays] (
  [h_id] int NOT NULL,
  [h_state] varchar(10) NOT NULL DEFAULT 'ALL',
  [h_style] char(1) DEFAULT 'S',
  [h_holiday] varchar(20) NOT NULL DEFAULT '',
  [h_name] varchar(64) DEFAULT '',
  PRIMARY KEY ([h_id])
) ;
BULK INSERT [holidays] FROM 'E:\ETL Processing\Gateway\Import\Extract\holidays.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors54.txt');




SELECT 'iccompany';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='iccompany') DROP TABLE [iccompany];
CREATE TABLE [iccompany] (
  [ic_importconfig] int NOT NULL DEFAULT '0',
  [ic_order] int NOT NULL DEFAULT '0',
  [ic_match] varchar(32) NOT NULL DEFAULT '',
  [ic_company_id] varchar(32) NOT NULL DEFAULT '',
) ;
BULK INSERT [iccompany] FROM 'E:\ETL Processing\Gateway\Import\Extract\iccompany.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors55.txt');




SELECT 'icfile';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='icfile') DROP TABLE [icfile];
CREATE TABLE [icfile] (
  [if_importconfig] int NOT NULL DEFAULT '0',
  [if_order] int NOT NULL DEFAULT '0',
  [if_match] varchar(32) NOT NULL DEFAULT '',
) ;
BULK INSERT [icfile] FROM 'E:\ETL Processing\Gateway\Import\Extract\icfile.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors56.txt');




SELECT 'importconfig';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='importconfig') DROP TABLE [importconfig];
CREATE TABLE [importconfig] (
  [ic_id] int NOT NULL,
  [ic_description] varchar(32) NOT NULL DEFAULT '',
  [ic_company_id] int NOT NULL DEFAULT '0',
  [ic_mechanism] char(1) DEFAULT 'F',
  [ic_style] char(1) DEFAULT 'C',
  [ic_format] varchar(32) NOT NULL DEFAULT '',
  [ic_default_insurance] char(1) DEFAULT '0',
  [ic_unique_connote] char(1) DEFAULT 'N',
  [ic_unique_timeout] int DEFAULT '366',
  [ic_update_connote] char(1) DEFAULT 'N',
  [ic_update_timeout] int DEFAULT '14',
  [ic_import_to_ezyfreight] char(1) DEFAULT 'N',
  [ic_validate_import] char(1) DEFAULT 'N',
  [ic_ftp_host] varchar(64) DEFAULT NULL,
  [ic_ftp_user] varchar(64) DEFAULT NULL,
  [ic_ftp_password] varchar(64) DEFAULT NULL,
  [ic_ftp_directory] varchar(64) DEFAULT NULL,
  [ic_ftp_archive] varchar(64) DEFAULT NULL,
  [ic_frequency] char(1) DEFAULT 'A',
  [ic_default_addr0] varchar(40) DEFAULT '',
  [ic_default_addr1] varchar(40) DEFAULT '',
  [ic_default_addr2] varchar(40) DEFAULT '',
  [ic_default_addr3] varchar(40) DEFAULT '',
  [ic_default_suburb] varchar(30) DEFAULT '',
  [ic_default_postcode] int DEFAULT '0',
  [ic_default_always] char(1) DEFAULT 'N',
  [ic_lastlog] varchar(128) DEFAULT NULL,
  [ic_laststamp] datetime default NULL,
  [ic_lastok] char(1) DEFAULT 'Y',
  [ic_lastfile] varchar(128) DEFAULT NULL,
  [ic_lastfilestamp] datetime default NULL,
  [ic_default_weight] FLOAT(25) DEFAULT '0',
  [ic_default_volume] FLOAT(25) DEFAULT '0',
  [ic_default_per_item] char(1) DEFAULT 'N',
  [ic_book_pickups] char(1) DEFAULT 'N',
  [ic_email_recipient] char(1) DEFAULT 'N',
  [ic_missing_alert] char(1) DEFAULT 'N',
  [ic_missing_time] int DEFAULT '0',
  [ic_missing_email] varchar(255) DEFAULT '',
  [ic_missing_name] varchar(255) DEFAULT '',
  [ic_missing_checked] datetime default NULL,
  PRIMARY KEY ([ic_id]),
) ;
BULK INSERT [importconfig] FROM 'E:\ETL Processing\Gateway\Import\Extract\importconfig.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors57.txt');




SELECT 'imports';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='imports') DROP TABLE [imports];
CREATE TABLE [imports] (
  [i_id] int NOT NULL,
  [i_style] char(1) DEFAULT 'C',
  [i_company] int DEFAULT NULL,
  [i_stamp] datetime default NULL,
  [i_filename] varchar(64) DEFAULT NULL,
  [i_consignments] int DEFAULT NULL,
  [i_references] int DEFAULT NULL,
  [i_coupons] int DEFAULT NULL,
  [i_bookins] int DEFAULT NULL,
  [i_date] SMALLDATETIME default NULL,
  [i_manifest_id] int DEFAULT '0',
  PRIMARY KEY ([i_id]),
) ;
BULK INSERT [imports] FROM 'E:\ETL Processing\Gateway\Import\Extract\imports.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors58.txt');




SELECT 'invoicestore';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='invoicestore') DROP TABLE [invoicestore];
CREATE TABLE [invoicestore] (
  [is_id] int NOT NULL,
  [is_account] char(32) NOT NULL DEFAULT '',
  [is_invoiceno] char(32) NOT NULL DEFAULT '',
  [is_date] SMALLDATETIME default NULL,
  [is_imported] datetime default NULL,
  PRIMARY KEY ([is_id]),
) ;
BULK INSERT [invoicestore] FROM 'E:\ETL Processing\Gateway\Import\Extract\invoicestore.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors59.txt');




SELECT 'irp';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='irp') DROP TABLE [irp];
CREATE TABLE [irp] (
  [irp_id] int NOT NULL,
  [irp_consignment] int NOT NULL DEFAULT '0',
  [irp_delivery_branch] int NOT NULL DEFAULT '0',
  [irp_delivery_phone] char(32) DEFAULT '',
  [irp_delivery_details] char(255) DEFAULT '',
  [irp_delivery_suburb_string] char(40) DEFAULT NULL,
  [irp_delivery_suburb] int DEFAULT '0',
  [irp_pickup_branch] int NOT NULL DEFAULT '0',
  [irp_pickup_phone] char(32) DEFAULT '',
  [irp_pickup_details] char(255) DEFAULT '',
  [irp_pickup_suburb_string] char(40) DEFAULT NULL,
  [irp_pickup_suburb] int DEFAULT '0',
  [irp_pickup_contact] char(32) DEFAULT '',
  [irp_pickup_location] char(32) DEFAULT '',
  [irp_pickup_date] SMALLDATETIME default NULL,
  [irp_pickup_time] time DEFAULT '00:00:00',
  [irp_job_no] int DEFAULT '0',
  [irp_job_date] SMALLDATETIME default NULL,
  [irp_coupon] char(32) DEFAULT '',
  [irp_coupon_id] int DEFAULT '0',
  [irp_tracker] char(32) DEFAULT '',
  [irp_tracker_id] int DEFAULT '0',
  [irp_dimension0] int DEFAULT '0',
  [irp_dimension1] int DEFAULT '0',
  [irp_dimension2] int DEFAULT '0',
  [irp_cube] int DEFAULT '0',
  [irp_deadweight] int DEFAULT '0',
  [irp_deleted] char(1) DEFAULT 'N',
  PRIMARY KEY ([irp_id]),
) ;
BULK INSERT [irp] FROM 'E:\ETL Processing\Gateway\Import\Extract\irp.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors60.txt');




SELECT 'irpcoupon';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='irpcoupon') DROP TABLE [irpcoupon];
CREATE TABLE [irpcoupon] (
  [ic_irp_id] int NOT NULL DEFAULT '0',
  [ic_coupon] char(32) DEFAULT '',
  [ic_coupon_id] int DEFAULT '0',
  [ic_tracker] char(1) DEFAULT 'Y'
) ;
BULK INSERT [irpcoupon] FROM 'E:\ETL Processing\Gateway\Import\Extract\irpcoupon.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors61.txt');




SELECT 'login';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='login') DROP TABLE [login];
CREATE TABLE [login] (
  [username] varchar(100) NOT NULL DEFAULT '',
  [password] varchar(100) NOT NULL DEFAULT '',
  [branch] varchar(100) NOT NULL DEFAULT '',
  [level] int NOT NULL DEFAULT '0',
  PRIMARY KEY ([username])
) ;
BULK INSERT [login] FROM 'E:\ETL Processing\Gateway\Import\Extract\login.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors62.txt');




SELECT 'manifest';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='manifest') DROP TABLE [manifest];
CREATE TABLE [manifest] (
  [m_id] int NOT NULL,
  [m_date] SMALLDATETIME default NULL,
  [m_company_id] int NOT NULL DEFAULT '0',
  [m_description] varchar(32) DEFAULT '',
  [m_comments] varchar(255) DEFAULT '',
  [m_consignments] int DEFAULT '0',
  [m_stamp] datetime default NULL,
  [m_class] int NOT NULL DEFAULT '0',
  [m_import_id] int DEFAULT NULL,
  [m_released] char(1) DEFAULT 'N',
  [m_printed] char(1) DEFAULT 'N',
  PRIMARY KEY ([m_id])
) ;
BULK INSERT [manifest] FROM 'E:\ETL Processing\Gateway\Import\Extract\manifest.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors63.txt');




SELECT 'noncosmosdriver';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='noncosmosdriver') DROP TABLE [noncosmosdriver];
CREATE TABLE [noncosmosdriver] (
  [nc_branch] int NOT NULL DEFAULT '0',
  [nc_contractor] int NOT NULL DEFAULT '0',
  [nc_description] varchar(64) DEFAULT NULL,
  PRIMARY KEY ([nc_branch],[nc_contractor])
) ;
BULK INSERT [noncosmosdriver] FROM 'E:\ETL Processing\Gateway\Import\Extract\noncosmosdriver.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors64.txt');




SELECT 'ogmanifest';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ogmanifest') DROP TABLE [ogmanifest];
CREATE TABLE [ogmanifest] (
  [ogm_id] int NOT NULL,
  [ogm_date] SMALLDATETIME default NULL,
  [ogm_stamp] datetime default NULL,
  [ogm_class] int NOT NULL DEFAULT '0',
  [ogm_agent] int NOT NULL DEFAULT '0',
  [ogm_count] int DEFAULT '0',
  [ogm_returns] char(1) DEFAULT 'N',
  PRIMARY KEY ([ogm_id]),
) ;
BULK INSERT [ogmanifest] FROM 'E:\ETL Processing\Gateway\Import\Extract\ogmanifest.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors65.txt');




SELECT 'ogmexceptions';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='ogmexceptions') DROP TABLE [ogmexceptions];
CREATE TABLE [ogmexceptions] (
  [oge_id] int NOT NULL,
  [oge_ogm_id] int NOT NULL DEFAULT '0',
  [oge_consignment_id] int NOT NULL DEFAULT '0',
  [oge_description] varchar(255) DEFAULT NULL,
  PRIMARY KEY ([oge_id])
) ;
BULK INSERT [ogmexceptions] FROM 'E:\ETL Processing\Gateway\Import\Extract\ogmexceptions.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors66.txt');




SELECT 'pcodebranch';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pcodebranch') DROP TABLE [pcodebranch];
CREATE TABLE [pcodebranch] (
  [pb_start] int NOT NULL DEFAULT '0',
  [pb_end] int NOT NULL DEFAULT '0',
  [pb_branch] int NOT NULL DEFAULT '0',
  PRIMARY KEY ([pb_start])
) ;
BULK INSERT [pcodebranch] FROM 'E:\ETL Processing\Gateway\Import\Extract\pcodebranch.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors67.txt');




SELECT 'price';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='price') DROP TABLE [price];
CREATE TABLE [price] (
  [p_id] int NOT NULL,
  [p_name] varchar(64) DEFAULT NULL,
  [p_action] char(1) DEFAULT 'N',
  [p_target] char(2) DEFAULT 'AD',
  [p_style] char(1) DEFAULT 'F',
  [p_cpplonly] char(1) DEFAULT 'N',
  [p_primary_cost] FLOAT(25) DEFAULT '0',
  [p_primary_break] FLOAT(25) DEFAULT '0',
  [p_secondary_cost] FLOAT(25) DEFAULT '0',
  [p_secondary_break] FLOAT(25) DEFAULT '0',
  [p_cubic_conversion] FLOAT(25) DEFAULT '250',
  PRIMARY KEY ([p_id])
) ;
BULK INSERT [price] FROM 'E:\ETL Processing\Gateway\Import\Extract\price.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors68.txt');




SELECT 'pricecodes';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pricecodes') DROP TABLE [pricecodes];
CREATE TABLE [pricecodes] (
  [pc_code] varchar(16) NOT NULL DEFAULT '',
  [pc_name] varchar(32) NOT NULL DEFAULT '',
  [pc_validation_id] int DEFAULT NULL,
  [pc_consolidation_code] varchar(16) DEFAULT '',
  [pc_default_weight] int DEFAULT '0',
  [pc_default_cube] FLOAT(25) DEFAULT '0',
  [pc_default_instruction] char(80) DEFAULT '',
  PRIMARY KEY ([pc_code])
) ;
BULK INSERT [pricecodes] FROM 'E:\ETL Processing\Gateway\Import\Extract\pricecodes.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors69.txt');




SELECT 'pricegroup';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pricegroup') DROP TABLE [pricegroup];
CREATE TABLE [pricegroup] (
  [pg_id] int NOT NULL,
  [pg_name] varchar(32) NOT NULL DEFAULT '',
  [pg_style] char(1) DEFAULT 'S',
  [pg_parent] int DEFAULT NULL,
  PRIMARY KEY ([pg_id])
) ;
BULK INSERT [pricegroup] FROM 'E:\ETL Processing\Gateway\Import\Extract\pricegroup.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors70.txt');




SELECT 'pricegroup2';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pricegroup2') DROP TABLE [pricegroup2];
CREATE TABLE [pricegroup2] (
  [pg_id] int NOT NULL DEFAULT '0',
  [pg_name] varchar(32) NOT NULL DEFAULT '',
  [pg_style] char(1) DEFAULT 'S',
  [pg_parent] int DEFAULT NULL
) ;
BULK INSERT [pricegroup2] FROM 'E:\ETL Processing\Gateway\Import\Extract\pricegroup2.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors71.txt');




SELECT 'pricezonemap';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pricezonemap') DROP TABLE [pricezonemap];
CREATE TABLE [pricezonemap] (
  [pz_zonemap] int NOT NULL DEFAULT '0',
  [pz_pricecode] varchar(16) NOT NULL DEFAULT '',
  [pz_fromzone] int NOT NULL DEFAULT '0',
  [pz_tozone] int NOT NULL DEFAULT '0',
  [pz_mapped] varchar(16) NOT NULL DEFAULT '',
) ;
BULK INSERT [pricezonemap] FROM 'E:\ETL Processing\Gateway\Import\Extract\pricezonemap.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors72.txt');




SELECT 'pwreset';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='pwreset') DROP TABLE [pwreset];
CREATE TABLE [pwreset] (
  [pr_id] int  NOT NULL,
  [pr_userid] int  NOT NULL,
  [pr_username] varchar(32) NOT NULL,
  [pr_email] varchar(256) NOT NULL,
  [pr_create_stamp] datetime default NULL,
  [pr_unique_id] varchar(32) NOT NULL,
  [pr_state] int DEFAULT '0',
  [pr_reset_stamp] datetime default NULL,
  PRIMARY KEY ([pr_id]),
) ;
BULK INSERT [pwreset] FROM 'E:\ETL Processing\Gateway\Import\Extract\pwreset.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors73.txt');




SELECT 'rate_data';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='rate_data') DROP TABLE [rate_data];
CREATE TABLE [rate_data] (
  [rd_id] int NOT NULL,
  [rd_rating] int DEFAULT NULL,
  [rd_action] char(1) DEFAULT 'N',
  [rd_style] char(1) DEFAULT 'F',
  [rd_data_id] int DEFAULT NULL,
  PRIMARY KEY ([rd_id])
) ;
BULK INSERT [rate_data] FROM 'E:\ETL Processing\Gateway\Import\Extract\rate_data.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors74.txt');




SELECT 'rating';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='rating') DROP TABLE [rating];
CREATE TABLE [rating] (
  [r_id] int NOT NULL,
  [r_company_id] int NOT NULL DEFAULT '0',
  [r_name] varchar(32) DEFAULT NULL,
  [r_description] varchar(32) DEFAULT NULL,
  PRIMARY KEY ([r_id])
) ;
BULK INSERT [rating] FROM 'E:\ETL Processing\Gateway\Import\Extract\rating.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors75.txt');




SELECT 'receipts';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='receipts') DROP TABLE [receipts];
CREATE TABLE [receipts] (
  [r_id] int NOT NULL,
  [r_company_id] int NOT NULL DEFAULT '0',
  [r_user_id] int NOT NULL DEFAULT '0',
  [r_date] SMALLDATETIME default NULL,
  [r_creation_stamp] datetime default NULL,
  [r_exclusive] FLOAT(25) DEFAULT '0',
  [r_gst] FLOAT(25) DEFAULT '0',
  [r_total] FLOAT(25) DEFAULT '0',
  [r_unallocated] FLOAT(25) DEFAULT '0',
  [r_type] char(1) DEFAULT '',
  [r_style] char(1) DEFAULT '',
  [r_closed] char(1) DEFAULT 'N',
  [r_reversed] char(1) DEFAULT 'N',
  [r_bank] varchar(32) DEFAULT NULL,
  [r_branch] varchar(32) DEFAULT NULL,
  [r_drawer] varchar(50) DEFAULT NULL,
  [r_no] varchar(32) DEFAULT NULL,
  [r_reference] varchar(32) DEFAULT NULL,
  [r_comment] varchar(64) DEFAULT NULL,
  PRIMARY KEY ([r_id])
) ;
BULK INSERT [receipts] FROM 'E:\ETL Processing\Gateway\Import\Extract\receipts.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors76.txt');




SELECT 'return_info';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='return_info') DROP TABLE [return_info];
CREATE TABLE [return_info] (
  [r_id] int NOT NULL,
  [r_company_id] int NOT NULL DEFAULT '0',
  [r_consignment] int NOT NULL DEFAULT '0',
  PRIMARY KEY ([r_id])
) ;
BULK INSERT [return_info] FROM 'E:\ETL Processing\Gateway\Import\Extract\return_info.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors77.txt');




SELECT 'salesrep';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='salesrep') DROP TABLE [salesrep];
CREATE TABLE [salesrep] (
  [sr_id] int NOT NULL,
  [sr_code] varchar(8) NOT NULL DEFAULT '',
  [sr_name] varchar(40) NOT NULL DEFAULT '',
  [sr_addr0] varchar(40) DEFAULT NULL,
  [sr_addr1] varchar(40) DEFAULT NULL,
  [sr_addr2] varchar(40) DEFAULT NULL,
  [sr_addr3] varchar(40) DEFAULT NULL,
  [sr_suburb] varchar(30) DEFAULT NULL,
  [sr_postcode] int DEFAULT '0',
  [sr_phone] varchar(16) DEFAULT NULL,
  [sr_started] SMALLDATETIME default NULL,
  [sr_terminated] SMALLDATETIME default NULL,
  PRIMARY KEY ([sr_id]),
) ;
BULK INSERT [salesrep] FROM 'E:\ETL Processing\Gateway\Import\Extract\salesrep.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors78.txt');




SELECT 'sessions';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='sessions') DROP TABLE [sessions];
CREATE TABLE [sessions] (
  [id] varchar(32) NOT NULL DEFAULT '',
  [access] int DEFAULT NULL,
  [data] text,
  PRIMARY KEY ([id])
) ;
BULK INSERT [sessions] FROM 'E:\ETL Processing\Gateway\Import\Extract\sessions.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors79.txt');




SELECT 'sortationdriver';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='sortationdriver') DROP TABLE [sortationdriver];
CREATE TABLE [sortationdriver] (
  [s_branch] int NOT NULL DEFAULT '0',
  [s_contractor] int NOT NULL DEFAULT '0',
  [s_description] varchar(64) DEFAULT NULL,
  PRIMARY KEY ([s_branch],[s_contractor])
) ;
BULK INSERT [sortationdriver] FROM 'E:\ETL Processing\Gateway\Import\Extract\sortationdriver.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors80.txt');




SELECT 'suburb';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='suburb') DROP TABLE [suburb];
CREATE TABLE [suburb] (
  [s_id] int NOT NULL,
  [s_cosmosid] int DEFAULT NULL,
  [s_branch] int DEFAULT NULL,
  [s_depot] int DEFAULT NULL,
  [s_driver] int DEFAULT NULL,
  [s_postcode] int DEFAULT NULL,
  [s_name] varchar(32) DEFAULT NULL,
  [s_state] varchar(10) DEFAULT NULL,
  [s_zone] varchar(10) DEFAULT NULL,
  PRIMARY KEY ([s_id]),
) ;
BULK INSERT [suburb] FROM 'E:\ETL Processing\Gateway\Import\Extract\suburb.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors81txt');




SELECT 'trackcompany';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='trackcompany') DROP TABLE [trackcompany];
CREATE TABLE [trackcompany] (
  [tc_track] int NOT NULL DEFAULT '0',
  [tc_company_id] int NOT NULL DEFAULT '0',
) ;
BULK INSERT [trackcompany] FROM 'E:\ETL Processing\Gateway\Import\Extract\trackcompany.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors82.txt');




SELECT 'trackkey';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='trackkey') DROP TABLE [trackkey];
CREATE TABLE [trackkey] (
  [t_id] int  NOT NULL,
  [t_key] varchar(32) NOT NULL DEFAULT '',
  [t_description] varchar(64) NOT NULL DEFAULT '',
  [t_includeeta] char(1) DEFAULT 'N',
  [t_limited] char(1) DEFAULT 'N',
  PRIMARY KEY ([t_id]),
) ;
BULK INSERT [trackkey] FROM 'E:\ETL Processing\Gateway\Import\Extract\trackkey.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors83.txt');




SELECT 'transactions';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='transactions') DROP TABLE [transactions];
CREATE TABLE [transactions] (
  [t_id] int NOT NULL,
  [t_company_id] int NOT NULL DEFAULT '0',
  [t_date] SMALLDATETIME default NULL,
  [t_creation_stamp] datetime default NULL,
  [t_exclusive] FLOAT(25) DEFAULT '0',
  [t_gst] FLOAT(25) DEFAULT '0',
  [t_total] FLOAT(25) DEFAULT '0',
  [t_unallocated] FLOAT(25) DEFAULT '0',
  [t_type] char(1) DEFAULT '',
  [t_billing_id] int DEFAULT NULL,
  [t_parent_id] int DEFAULT NULL,
  [t_receipt_id] int DEFAULT NULL,
  [t_open] char(1) DEFAULT 'N',
  PRIMARY KEY ([t_id])
) ;
BULK INSERT [transactions] FROM 'E:\ETL Processing\Gateway\Import\Extract\transactions.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors84.txt');




SELECT 'type';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='type') DROP TABLE [type];
CREATE TABLE [type] (
  [ename] varchar(100) NOT NULL DEFAULT '',
  [ediscription] varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY ([ename])
) ;
BULK INSERT [type] FROM 'E:\ETL Processing\Gateway\Import\Extract\type.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors85.txt');




SELECT 'uctmp';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='uctmp') DROP TABLE [uctmp];
CREATE TABLE [uctmp] (
  [uc_coupon] char(40) NOT NULL DEFAULT '',
  [uc_action] char(20) NOT NULL DEFAULT '',
  [uc_stamp] datetime default NULL,
  [uc_company_id] int NOT NULL DEFAULT '0',
  [uc_contractor] int DEFAULT '0',
  [uc_branch] char(20) DEFAULT ''
) ;
BULK INSERT [uctmp] FROM 'E:\ETL Processing\Gateway\Import\Extract\uctmp.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors86.txt');




SELECT 'unknowncoupons';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='unknowncoupons') DROP TABLE [unknowncoupons];
CREATE TABLE [unknowncoupons] (
  [uc_coupon] char(40) NOT NULL DEFAULT '',
  [uc_action] char(20) NOT NULL DEFAULT '',
  [uc_stamp] datetime default NULL,
  [uc_company_id] int NOT NULL DEFAULT '0',
  [uc_contractor] int DEFAULT '0',
  [uc_branch] char(20) DEFAULT '',
) ;
BULK INSERT [unknowncoupons] FROM 'E:\ETL Processing\Gateway\Import\Extract\unknowncoupons.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors87.txt');




SELECT 'unknownwork';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='unknownwork') DROP TABLE [unknownwork];
CREATE TABLE [unknownwork] (
  [uc_coupon] char(40) NOT NULL DEFAULT '',
  [uc_action] char(20) NOT NULL DEFAULT '',
  [uc_stamp] datetime default NULL,
  [uc_company_id] int NOT NULL DEFAULT '0',
  [uc_contractor] int DEFAULT '0',
  [uc_branch] char(20) DEFAULT '',
) ;
BULK INSERT [unknownwork] FROM 'E:\ETL Processing\Gateway\Import\Extract\unknownwork.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors88.txt');




SELECT 'useragents';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='useragents') DROP TABLE [useragents];
CREATE TABLE [useragents] (
  [user_id] int NOT NULL DEFAULT '0',
  [agent_id] int NOT NULL DEFAULT '0',
) ;
BULK INSERT [useragents] FROM 'E:\ETL Processing\Gateway\Import\Extract\useragents.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors89.txt');




SELECT 'userclass';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='userclass') DROP TABLE [userclass];
CREATE TABLE [userclass] (
  [uc_user] int NOT NULL DEFAULT '0',
  [uc_class] int NOT NULL DEFAULT '0',
  [uc_email_manifest] char(1) DEFAULT 'N',
  [uc_email] varchar(255) DEFAULT NULL,
) ;
BULK INSERT [userclass] FROM 'E:\ETL Processing\Gateway\Import\Extract\userclass.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors90.txt');




SELECT 'usercompany';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='usercompany') DROP TABLE [usercompany];
CREATE TABLE [usercompany] (
  [user_id] int NOT NULL DEFAULT '0',
  [company_id] int NOT NULL DEFAULT '0',
  [email_manifest] char(1) DEFAULT 'N',
  [email] varchar(255) DEFAULT NULL,
) ;
BULK INSERT [usercompany] FROM 'E:\ETL Processing\Gateway\Import\Extract\usercompany.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors91.txt');




SELECT 'users';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='users') DROP TABLE [users];
CREATE TABLE [users] (
  [id] int  NOT NULL,
  [account] varchar(32) NOT NULL DEFAULT '',
  [salt] char(64) DEFAULT '',
  [md5] char(64) DEFAULT NULL,
  [fullname] varchar(64) DEFAULT NULL,
  [email] varchar(256) DEFAULT NULL,
  [lastlogin] datetime default NULL,
  [userlevel] int DEFAULT '0',
  [flags] int DEFAULT '0',
  [pReturns] char(1) DEFAULT 'N',
  [pAgents] char(1) DEFAULT 'N',
  [pAdmin] char(1) DEFAULT 'N',
  [pPricing] char(1) DEFAULT 'N',
  [pMaintenance] char(1) DEFAULT 'N',
  [pBilling] char(1) DEFAULT 'N',
  [pTracking] char(1) DEFAULT 'N',
  [pBookin] char(1) DEFAULT 'N',
  [pFinancial] char(1) DEFAULT 'N',
  [pSupervisor] char(1) DEFAULT 'N',
  [pPODEntry] char(1) DEFAULT 'N',
  [pConnote] char(1) DEFAULT 'N',
  [pUndelivered] char(1) DEFAULT 'N',
  [pInvoiceReprint] char(1) DEFAULT 'N',
  [pSplitDeliveries] char(1) DEFAULT 'N',
  [pDaily] char(1) DEFAULT 'N',
  [pTRAP] char(1) DEFAULT 'N',
  [pMonitor] char(1) DEFAULT 'N',
  [pNoStats] char(1) DEFAULT 'N',
  [pEzyFreightConsignment] char(1) DEFAULT 'N',
  [pEzyFreightLabels] char(1) DEFAULT 'N',
  [pEzyFreightPickup] char(1) DEFAULT 'N',
  [pEzyFreightImport] char(1) DEFAULT 'N',
  [pEzyFreightPricing] char(1) DEFAULT 'N',
  [pStaff] char(1) DEFAULT 'N',
  [pControlledReport] char(1) DEFAULT 'N',
  [fCSV] char(1) DEFAULT 'N',
  [contractor_number] int DEFAULT '0',
  [contractor_branch] int DEFAULT '0',
  [fContractor] char(1) DEFAULT 'N',
  [pEMM] char(1) DEFAULT 'N',
  [emm_branch] int DEFAULT '0',
  [pwchanged] SMALLDATETIME default NULL,
  [pwexempt] char(1) DEFAULT 'N',
  [disabled] char(1) DEFAULT 'N',
  [pAPConnote] char(1) DEFAULT 'N',
  [pEbay] char(1) DEFAULT 'N',
  PRIMARY KEY ([id]),
) ;
BULK INSERT [users] FROM 'E:\ETL Processing\Gateway\Import\Extract\users.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors92.txt');




SELECT 'userwarehouse';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='userwarehouse') DROP TABLE [userwarehouse];
CREATE TABLE [userwarehouse] (
  [uw_user_id] int NOT NULL DEFAULT '0',
  [uw_warehouse_id] int NOT NULL DEFAULT '0',
  [uw_email_manifest] char(1) DEFAULT 'N',
  [uw_email] varchar(255) DEFAULT NULL,
) ;
BULK INSERT [userwarehouse] FROM 'E:\ETL Processing\Gateway\Import\Extract\userwarehouse.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors93.txt');




SELECT 'utest';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='utest') DROP TABLE [utest];
CREATE TABLE [utest] (
  [id] int  NOT NULL DEFAULT '0',
  [account] varchar(32) NOT NULL DEFAULT '',
  [password] varchar(32) NOT NULL DEFAULT '',
  [encrypted] varchar(32) NOT NULL DEFAULT '',
  [md5] char(64) DEFAULT NULL,
  [fullname] varchar(64) DEFAULT NULL,
  [email] varchar(256) DEFAULT NULL,
  [lastlogin] datetime default NULL,
  [userlevel] int DEFAULT '0',
  [flags] int DEFAULT '0',
  [pReturns] char(1) DEFAULT 'N',
  [pAgents] char(1) DEFAULT 'N',
  [pAdmin] char(1) DEFAULT 'N',
  [pPricing] char(1) DEFAULT 'N',
  [pMaintenance] char(1) DEFAULT 'N',
  [pBilling] char(1) DEFAULT 'N',
  [pTracking] char(1) DEFAULT 'N',
  [pBookin] char(1) DEFAULT 'N',
  [pFinancial] char(1) DEFAULT 'N',
  [pSupervisor] char(1) DEFAULT 'N',
  [pPODEntry] char(1) DEFAULT 'N',
  [pConnote] char(1) DEFAULT 'N',
  [pUndelivered] char(1) DEFAULT 'N',
  [pInvoiceReprint] char(1) DEFAULT 'N',
  [pSplitDeliveries] char(1) DEFAULT 'N',
  [pDaily] char(1) DEFAULT 'N',
  [pTRAP] char(1) DEFAULT 'N',
  [pMonitor] char(1) DEFAULT 'N',
  [pNoStats] char(1) DEFAULT 'N',
  [pEzyFreightConsignment] char(1) DEFAULT 'N',
  [pEzyFreightLabels] char(1) DEFAULT 'N',
  [pEzyFreightPickup] char(1) DEFAULT 'N',
  [pStaff] char(1) DEFAULT 'N',
  [pControlledReport] char(1) DEFAULT 'N',
  [fCSV] char(1) DEFAULT 'N',
  [contractor_number] int DEFAULT '0',
  [contractor_branch] int DEFAULT '0',
  [fContractor] char(1) DEFAULT 'N',
  [pEMM] char(1) DEFAULT 'N',
  [emm_branch] int DEFAULT '0',
  [pwchanged] SMALLDATETIME default NULL,
  [pwexempt] char(1) DEFAULT 'N',
  [disabled] char(1) DEFAULT 'N',
  [pAPConnote] char(1) DEFAULT 'N'
) ;
BULK INSERT [utest] FROM 'E:\ETL Processing\Gateway\Import\Extract\utest.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors94.txt');




SELECT 'validation';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='validation') DROP TABLE [validation];
CREATE TABLE [validation] (
  [v_id] int NOT NULL,
  [v_minimum_deadweight_required] char(1) DEFAULT 'N',
  [v_minimum_deadweight_value] FLOAT(25) DEFAULT NULL,
  [v_minimum_volume_required] char(1) DEFAULT 'N',
  [v_minimum_volume_value] FLOAT(25) DEFAULT '0',
  [v_minimum_ratio_required] char(1) DEFAULT 'N',
  [v_minimum_ratio_value] FLOAT(25) DEFAULT '0',
  [v_minimum_items_required] char(1) DEFAULT 'N',
  [v_minimum_items_value] int DEFAULT '0',
  [v_maximum_deadweight_required] char(1) DEFAULT 'N',
  [v_maximum_deadweight_value] FLOAT(25) DEFAULT NULL,
  [v_maximum_volume_required] char(1) DEFAULT 'N',
  [v_maximum_volume_value] FLOAT(25) DEFAULT '0',
  [v_maximum_ratio_required] char(1) DEFAULT 'N',
  [v_maximum_ratio_value] FLOAT(25) DEFAULT '0',
  [v_maximum_items_required] char(1) DEFAULT 'N',
  [v_maximum_items_value] int DEFAULT '0',
  [v_no_suburb_validation] char(1) DEFAULT 'N',
  PRIMARY KEY ([v_id])
) ;
BULK INSERT [validation] FROM 'E:\ETL Processing\Gateway\Import\Extract\validation.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors95.txt');




SELECT 'warehouse';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='warehouse') DROP TABLE [warehouse];
CREATE TABLE [warehouse] (
  [w_id] int NOT NULL,
  [w_name] varchar(32) NOT NULL DEFAULT '',
  [w_description] varchar(64) DEFAULT NULL,
  PRIMARY KEY ([w_id]),
) ;
BULK INSERT [warehouse] FROM 'E:\ETL Processing\Gateway\Import\Extract\warehouse.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors96.txt');




SELECT 'x1';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='x1') DROP TABLE [x1];
CREATE TABLE [x1] (
  [ccg_pricegroup] int NOT NULL DEFAULT '0',
  [ccg_pricecode] varchar(16) NOT NULL DEFAULT '',
  [ccg_fromzone] int NOT NULL DEFAULT '0',
  [ccg_tozone] int NOT NULL DEFAULT '0',
  [ccg_present] char(1) DEFAULT 'N',
  [ccg_baseprice] FLOAT(25) DEFAULT '0',
  [ccg_15kg] FLOAT(25) DEFAULT '0',
  [ccg_25kg] FLOAT(25) DEFAULT '0',
  [ccg_pickup_payamount] FLOAT(25) DEFAULT '0',
  [ccg_pickup_15kg] FLOAT(25) DEFAULT '0',
  [ccg_pickup_25kg] FLOAT(25) DEFAULT '0',
  [ccg_deliver_payamount] FLOAT(25) DEFAULT '0',
  [ccg_deliver_15kg] FLOAT(25) DEFAULT '0',
  [ccg_deliver_25kg] FLOAT(25) DEFAULT '0'
) ;
BULK INSERT [x1] FROM 'E:\ETL Processing\Gateway\Import\Extract\x1.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors97.txt');




SELECT 'zonegrid';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zonegrid') DROP TABLE [zonegrid];
CREATE TABLE [zonegrid] (
  [zp_fromzone] int NOT NULL DEFAULT '0',
  [zp_tozone] int NOT NULL DEFAULT '0',
  [zp_price] int NOT NULL DEFAULT '0',
  [zp_pricegroup_id] int NOT NULL DEFAULT '0',
  [zp_multiplier] int DEFAULT '1',
  [zp_pricecode] varchar(16) NOT NULL DEFAULT 'STD',
  PRIMARY KEY ([zp_fromzone],[zp_tozone],[zp_price],[zp_pricegroup_id],[zp_pricecode])
) ;
BULK INSERT [zonegrid] FROM 'E:\ETL Processing\Gateway\Import\Extract\zonegrid.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors98.txt');




SELECT 'zonemaps';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zonemaps') DROP TABLE [zonemaps];
CREATE TABLE [zonemaps] (
  [zm_id] int NOT NULL,
  [zm_name] varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY ([zm_id])
) ;
BULK INSERT [zonemaps] FROM 'E:\ETL Processing\Gateway\Import\Extract\zonemaps.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors99.txt');




SELECT 'zoneprice';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zoneprice') DROP TABLE [zoneprice];
CREATE TABLE [zoneprice] (
  [zp_zone] int NOT NULL DEFAULT '0',
  [zp_price] int NOT NULL DEFAULT '0',
  [zp_company] int NOT NULL DEFAULT '0',
  [zp_multiplier] int DEFAULT '1',
  [zp_pricecode] varchar(16) DEFAULT 'STD',
  PRIMARY KEY ([zp_zone],[zp_price],[zp_company])
) ;
BULK INSERT [zoneprice] FROM 'E:\ETL Processing\Gateway\Import\Extract\zoneprice.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors100.txt');




SELECT 'zones';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zones') DROP TABLE [zones];
CREATE TABLE [zones] (
  [z_id] int NOT NULL,
  [z_code] varchar(16) DEFAULT NULL,
  [z_name] varchar(32) DEFAULT NULL,
  [z_warehouse_id] int NOT NULL DEFAULT '0',
  [z_level] char(1) DEFAULT '3',
  [z_order] int DEFAULT '1000',
  [z_color] varchar(64) DEFAULT NULL,
  PRIMARY KEY ([z_id])
) ;
BULK INSERT [zones] FROM 'E:\ETL Processing\Gateway\Import\Extract\zones.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors.txt101');




SELECT 'zoning';
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='zoning') DROP TABLE [zoning];
CREATE TABLE [zoning] (
  [z_id] int NOT NULL,
  [z_name] varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY ([z_id])
) ;
BULK INSERT [zoning] FROM 'E:\ETL Processing\Gateway\Import\Extract\zoning.txt' WITH (FIELDTERMINATOR = '|~!', ROWTERMINATOR='\n',TABLOCK, MAXERRORS=99999, ERRORFILE='E:\ETL Processing\Gateway\Import\Errors\cpplEDI_insert_errors102.txt');




--exec PDG_Create_FK_Constraints;

dbcc shrinkfile('cpplEDI_log',1);

END
GO


